DROP TABLE IF EXISTS MediciS;

CREATE TABLE MediciS (
  email varchar(30) PRIMARY KEY NOT NULL,
  password varchar(30) NOT NULL,
  nome varchar(30) NOT NULL,
  cognome varchar(30) NOT NULL,
  citta varchar(30) NOT NULL,
  provincia char(2) NOT NULL,
  matricola varchar(30) NOT NULL,
  FOREIGN KEY (provincia) REFERENCES Ssp (provincia)
)

INSERT INTO MediciS VALUES
('gianlucabaggio@gmail.com','Iwannabe1astronaut','Gianluca','Baggio','Bassano del Grappa','VI','VI10001'),
('iu3byv@gmail.com','CiaoMS','Test','Test','Schio','VI','VI10004'),
('marialauretta@gmail.com','ADGzcb123','Maria','Lauretta','Padova','PD','PD10001');
