DROP TABLE IF EXISTS Admin;

CREATE TABLE Admin (
  id varchar(30) PRIMARY KEY NOT NULL,
  password varchar(40) NOT NULL,
  id_prov char(2) NOT NULL,
  FOREIGN KEY (id_prov) REFERENCES Ssp (provincia)
)

INSERT INTO Admin VALUES
('adm00001','pwadm00001','VI'),
('adm00002','pwadm00002','PD');
