DROP TABLE IF EXISTS Esami;

CREATE TABLE Esami (
  id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  nome varchar(200) NOT NULL,
  descrizione varchar(200) NOT NULL
)

INSERT INTO Esami VALUES
(1,'Esame del Sangue','controllo dei valori standard'),
(2,'Radiografia','utilizzo dei raggi per evidenziare criticita'),
(3,'Esame delle Urine','controllo dei valori standard'),
(4,'Colonscopia','identificazione di criticita relative al colon'),
(5,'Esame acido citrico','controllo dei valori standard di acido citrico'),
(6,'Esame acido acetoacetico','controllo dei valori standard di acido acetoacetico'),
(7,'Esame acido ippurico','controllo dei valori standard di acido ippurico'),
(8,'Esame acido lattico','controllo dei valori standard di acido lattico'),
(9,'Esame acido valproico','controllo dei valori standard di acido valproico'),
(10,'Esame acido sialico libero','controllo dei valori standard di acido sialico libero'),
(11,'Esame acido sialico totale','controllo dei valori standard di acido sialico totale');
