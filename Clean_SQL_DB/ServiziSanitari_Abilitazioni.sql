DROP TABLE IF EXISTS Abilitazioni;

CREATE TABLE Abilitazioni (
  id_medicos varchar(30) NOT NULL,
  id_esame int(11) NOT NULL,
  PRIMARY KEY (id_medicos,id_esame),
  FOREIGN KEY (id_medicos) REFERENCES MediciS (email),
  FOREIGN KEY (id_esame) REFERENCES Esami (id)
)
/*ENGINE=InnoDB DEFAULT CHARSET=latin1;*/

INSERT INTO Abilitazioni VALUES
('iu3byv@gmail.com',1),
('iu3byv@gmail.com',2),
('iu3byv@gmail.com',3),
('iu3byv@gmail.com',4),
('gianlucabaggio@gmail.com',5),
('gianlucabaggio@gmail.com',6),
('gianlucabaggio@gmail.com',7),
('gianlucabaggio@gmail.com',8),
('marialauretta@gmail.com',9),
('marialauretta@gmail.com',10),
('iu3byv@gmail.com',11),
('marialauretta@gmail.com',11);
