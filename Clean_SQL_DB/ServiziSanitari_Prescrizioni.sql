DROP TABLE IF EXISTS Prescrizioni;

CREATE TABLE Prescrizioni (
  id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  id_esame int(11) NOT NULL,
  id_paziente varchar(30) NOT NULL,
  referto text,
  id_medicoB varchar(30) NOT NULL,
  id_medicoS varchar(30) DEFAULT NULL,
  erogato boolean NOT NULL DEFAULT false,
  ticket boolean NOT NULL DEFAULT false,
  dataV date DEFAULT NULL,
  data date DEFAULT NULL,
  FOREIGN KEY (id_esame) REFERENCES Esami (id),
  FOREIGN KEY (id_medicoS) REFERENCES MediciS (email),
  FOREIGN KEY (id_medicoB) REFERENCES MediciB (email),
  FOREIGN KEY (id_paziente) REFERENCES Pazienti (email)
)

INSERT INTO Prescrizioni VALUES
(1,8,'iu3byv@gmail.com',NULL,'iu3byv@gmail.com',NULL,0,0,'2019-12-20',NULL),
(2,1,'iu3byv@gmail.com','222222','iu3byv@gmail.com','iu3byv@gmail.com',1,0,'2019-12-20','2019-12-20'),
(3,1,'balasso.andrea69@gmail.com',NULL,'giuliogallo@gmail.com',NULL,0,0,'2019-12-20',NULL),
(4,3,'iu3byv@gmail.com','11111','iu3byv@gmail.com','iu3byv@gmail.com',1,1,'2019-12-20','2019-12-20'),
(5,3,'iu3byv@gmail.com',NULL,'iu3byv@gmail.com',NULL,0,0,'2019-12-20',NULL),
(6,11,'iu3byv@gmail.com','ok tick','iu3byv@gmail.com','iu3byv@gmail.com',1,1,'2019-12-21','2019-12-21'),
(7,1,'iu3byv@gmail.com','ok prov','iu3byv@gmail.com','iu3byv@gmail.com',1,0,'2019-12-21','2019-12-21'),
(8,2,'balasso.andrea69@gmail.com',NULL,'giuliogallo@gmail.com',NULL,0,0,'2019-12-21',NULL);
