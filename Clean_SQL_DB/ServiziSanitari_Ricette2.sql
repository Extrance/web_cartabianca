DROP TABLE IF EXISTS Ricette2;

CREATE TABLE Ricette2 (
  id_ric int(11) NOT NULL,
  id_farmaco int(11) NOT NULL,
  quantita int(11) NOT NULL,
  PRIMARY KEY (id_ric,id_farmaco),
  FOREIGN KEY (id_ric) REFERENCES RicTot (id),
  FOREIGN KEY (id_farmaco) REFERENCES Farmaci (id)
)

INSERT INTO Ricette2 VALUES
(1,1,1),
(1,3,1),
(2,1,2),
(2,2,1),
(2,3,2),
(2,4,3),
(3,1,1),
(3,2,2),
(3,3,3),
(3,4,4),
(4,2,2),
(5,3,3);
