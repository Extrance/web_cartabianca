DROP TABLE IF EXISTS RicTot;

CREATE TABLE RicTot (
  id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  id_medico varchar(30) NOT NULL,
  id_paziente varchar(30) NOT NULL,
  descrizione text,
  data date NOT NULL,
  erogata boolean NOT NULL DEFAULT false,
  FOREIGN KEY (id_medico) REFERENCES MediciB (email),
  FOREIGN KEY (id_paziente) REFERENCES Pazienti (email)
)

INSERT INTO RicTot VALUES
(1,'iu3byv@gmail.com','iu3byv@gmail.com','','2019-12-20',0),
(2,'iu3byv@gmail.com','iu3byv@gmail.com','','2019-12-20',0),
(3,'iu3byv@gmail.com','iu3byv@gmail.com','ok','2019-12-20',0),
(4,'iu3byv@gmail.com','iu3byv@gmail.com','','2019-12-21',0),
(5,'iu3byv@gmail.com','iu3byv@gmail.com','','2019-12-21',0);
