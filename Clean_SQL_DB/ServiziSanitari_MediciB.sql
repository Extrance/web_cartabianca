DROP TABLE IF EXISTS MediciB;

CREATE TABLE MediciB (
  email varchar(30) PRIMARY KEY NOT NULL,
  password varchar(40) NOT NULL,
  nome varchar(30) NOT NULL,
  cognome varchar(30) NOT NULL,
  citta varchar(30) NOT NULL,
  provincia char(2) NOT NULL,
  matricola varchar(30) NOT NULL,
  FOREIGN KEY (provincia) REFERENCES Ssp (provincia)
)

INSERT INTO MediciB VALUES
('francescosandri@gmail.com','migliorMEDICO1964','Francesco','Sandri','Piombino Dese','PD','PD00001'),
('giuliogallo@gmail.com','MastroLindo57','Giulio','Gallo','Romano di Ezzelino','VI','VI00001'),
('iu3byv@gmail.com','CiaoMB','Andrea','Balasso','Schio','VI','VI00004'),
('lucacarta@gmail.com','darkGAMER967','Luca','Carta','Marostica','VI','VI00002'),
('marcorossi@gmail.com','PINCOpallino25','Marco','Rossi','Bassano del Grappa','VI','VI00003'),
('robertafarina@gmail.com','auroraMARIO1228','Roberta','Farina','San Martino di Lupari','PD','PD00002');
