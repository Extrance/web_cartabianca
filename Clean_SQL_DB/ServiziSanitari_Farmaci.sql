DROP TABLE IF EXISTS Farmaci;

CREATE TABLE Farmaci (
  id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  nome varchar(50) NOT NULL,
  descrizione varchar(200) DEFAULT NULL
)

INSERT INTO Farmaci VALUES
(1,'aspirina','farmaco generico'),
(2,'tachipirina','farmaco generico'),
(3,'antipiretico','farmaco generico'),
(4,'antistaminico','farmaco generico');
