DROP TABLE IF EXISTS Visita;

CREATE TABLE Visita (
  id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  id_medico varchar(30) NOT NULL,
  id_paziente varchar(30) NOT NULL,
  note text,
  data date NOT NULL,
  FOREIGN KEY (id_medico) REFERENCES MediciB (email),
  FOREIGN KEY (id_paziente) REFERENCES Pazienti (email)
)

INSERT INTO Visita VALUES
(32,'iu3byv@gmail.com','iu3byv@gmail.com','aaa','2019-12-20'),
(33,'iu3byv@gmail.com','iu3byv@gmail.com','aaadsaghrstj','2019-12-20');
