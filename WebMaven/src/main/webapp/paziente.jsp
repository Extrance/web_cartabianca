<%-- 
    Document   : paziente
    Created on : 29-mag-2019, 15.05.53
    Author     : andreabalasso
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.PazienteDAO"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.DAOCreate"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Paziente"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Servizi sanitari - Paziente</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel = "icon" href ="img/transparent.png" type = "image/png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="css/sidenav.css" type="text/css"/>
    <style>
      body{
        background-image: url(img/background.jpg);
        background-position: top;
        background-attachment: fixed;
        background-repeat: no-repeat;
      }
    </style>
  </head>
  <body>
    
    <% if(session.getAttribute("ruolo")!=null && session.getAttribute("ruolo").equals("Paziente")) { %>
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="img/transparent.png" width="40px" style="margin-top: -9.5px; "></a>
          </div>
          <div class="collapse navbar-collapse"  style="padding-left:30px;" id="myNavbar">
            <ul class="nav navbar-nav">
              <li><a href="/">Chi siamo</a></li>
              <li><a href="/#section2">Servizi</a></li>
              <li><a href="/#section3">Dove trovarci</a></li>
              <li>
                <form class="navbar-form navbar-left" style="max-width: 250px;" action="CercaEsame" method="POST">  <!--DA FARE-->
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Cerca esame" name="esame">
                    <div class="input-group-btn">
                      <button class="btn btn-default" type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </li>
            </ul>
            <div class="nav navbar-nav navbar-right" style="padding-right:10px;">
              <ul class="nav navbar-nav navbar-right">
                <li>
                  <form action="PazientePage" method="GET">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="Paziente">
                      <span class="glyphicon glyphicon-user"></span>
                    </button>
                  </form>
                </li>
                <li>
                  <form action="Slog" method="POST">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="LogOut">
                      <span class="glyphicon glyphicon-log-out"></span> Logout
                    </button>
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    <% } %>
    
    <div class="container-fluid text-center" style="margin-top:50px">  
      <%
        try {
          PazienteDAO utd = DAOCreate.getUtenteDAO();
          Paziente req = utd.getUtenteByEmail((String) session.getAttribute("username"));
        
          if (req==null) {    //no match found
      %>
        <div class="col-sm-12 text-left"> 
          <h1>Attenzione: Login come paziente non effettuato</h1>
          <p>Si prega di effettuare il login per accedere al contenuto di questa pagina</p>
          <a href="/" class="btn btn-link">Menu Principale</a>
        </div>
      <% } else { %>
        <div class="row content">
          <div class="col-sm-2 sidenav" style="font-family: sans-serif;">
            <div style="text-align: center;">
              <br>
              <p><a href="EsamiPaziente" style="text-decoration: none;"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Esami</a></p>
              <p><a href="RicettePaziente" style="text-decoration: none;"><span class="glyphicon glyphicon-duplicate"></span>&nbsp;&nbsp;Ricette</a></p>
              <p><a href="VisitePaziente" style="text-decoration: none;"><span class="glyphicon glyphicon-calendar"></span>&nbsp;&nbsp;Visite</a></p>
              <p><a href="TicketPaziente" style="text-decoration: none;"><span class="glyphicon glyphicon-credit-card"></span>&nbsp;&nbsp;Ticket</a></p>
              <hr>
              <p><a href="Opzioni" style="text-decoration: none;"><span class="glyphicon glyphicon-cog"></span>&nbsp;&nbsp;Opzioni</a></p>
            </div>
          </div>
          <div class="col-sm-10 text-left contentView">
            <div class="row">
              <div class="col-sm-3 text-center" style="padding: 30px;">
                <img src="img/users/<%= req.getFoto() %>.jpg" alt="Profile picture" style="width:128px;height:128px;">
              </div>
              <div class="col-sm-9 text-left">
                <h1><%= req.getCognome()%> <%= req.getNome()%></h1>
                <p style="padding-left: 10px;">
                  <span style="color: blue;">Mail</span>&nbsp;<%= req.getEmail()%><br>
                  <span style="color: blue;">CF</span>&nbsp;&nbsp;&nbsp;<%= req.getCf()%>
                </p>
                <hr>
                <p style="padding-left: 10px;"><%= req.getDataNascita()%> (<%= req.getLuogoNascita()%>)</p>
              </div>
            </div>
            <div class="row" style="padding: 20px;">
              <hr>
              <b>Medico di base attuale:</b>
              <%
                Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
                Connection conn = db.getConnection();
                PreparedStatement st = conn.prepareStatement("SELECT DISTINCT M.nome, M.cognome FROM MediciB AS M, Pazienti AS P WHERE P.id_medico= ? AND M.email=P.id_medico");
                st.setString(1, (String) req.getMedico());
                ResultSet rs = st.executeQuery();
                while(rs.next()) {
              %>
                <%= rs.getString(1)%> <%= rs.getString(2)%> - (<%= req.getMedico()%>)
              <%}%>
              <br><br><br>
            </div>
          </div>
        </div>
      <% }
        }
        catch(Exception e) {
          response.sendRedirect("/");
        }
      %>   
    </div>
    
    <nav class="navbar navbar-inverse navbar-fixed-bottom">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">Servizi Sanitari</a>
        </div>
      </div>
    </nav>      
  </body>
</html>
