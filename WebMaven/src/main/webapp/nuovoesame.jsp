<%-- 
    Document   : nuovoesame
    Created on : 9-nov-2019, 0.26.26
    Author     : andreabalasso
--%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.MedicoB"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.MedicoBDAO"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.PazienteDAO"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.DAOCreate"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Paziente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Servizi sanitari - Nuovo esame</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="icon" href="img/transparent.png" type="image/png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="css/sidenav.css" type="text/css"/>
    <link rel="stylesheet" href="css/autocompleteCss.css" type="text/css"/>
    <script type="text/javascript" src="js/autocompleteFile.js"></script>
    <style>
      body{
        background-image: url(img/background.jpg);
        background-position: top;
        background-attachment: fixed;
        background-repeat: no-repeat;
      }

      .btn-link:hover {
        opacity: 0.5;
      }
      
      @media screen and (min-width: 1050px) {
        .customInline {
           display: inline-block;
        }
      }
    </style>
  </head>
  <body onload="fillForm();">
  
    <% if(session.getAttribute("ruolo")!=null && session.getAttribute("ruolo").equals("Medico di Base")) { %>
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="img/transparent.png" width="40px" style="margin-top: -9.5px; "></a>
          </div>
          <div class="collapse navbar-collapse"  style="padding-left:30px;" id="myNavbar">
            <ul class="nav navbar-nav">
              <li><a href="/#section1">Chi siamo</a></li>
              <li><a href="/#section2">Servizi</a></li>
              <li><a href="/#section3">Dove trovarci</a></li>
              <li>
                <form class="navbar-form navbar-left" autocomplete="off" style="max-width: 250px;" action="RefertoPaziente" method="POST">
                  <div class="input-group autocomplete">
                    <input id="myInput" type="text" class="form-control" placeholder="Cerca paziente" name="paziente">
                    <div class="input-group-btn">
                      <button class="btn btn-default" type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </li>
            </ul>
            <div class="nav navbar-nav navbar-right" style="padding-right:10px;">
              <ul class="nav navbar-nav navbar-right">
                <li>
                  <form action="MedicoBasePage" method="GET">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="Medico di Base">
                      <span class="glyphicon glyphicon-user"></span>
                    </button>
                  </form>  
                </li>
                <li>
                  <form action="Slog" method="POST">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="LogOut">
                      <span class="glyphicon glyphicon-log-out"></span> Logout
                    </button>
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    <% } %>
    
    <%  if(session.getAttribute("errorMessage")!=null) { %>
      <script>
        window.alert("Errore in fase di elaborazione della richiesta: "+"<%= session.getAttribute("errorMessage")%>");
      </script>
    <%
          session.setAttribute("errorMessage", null);
        }
    %>
    
    <div class="container-fluid text-center" style="margin-top:50px">  
      <%
        try {
          MedicoBDAO utd = DAOCreate.getMedicoBDAO();
          MedicoB req = utd.getUtenteByEmail((String) session.getAttribute("username"));
        
          if (req==null) {    //no match found
      %>
        <div class="col-sm-12 text-left"> 
          <h1>Attenzione: Login come medico di base non effettuato</h1>
          <p>Si prega di effettuare il login per accedere al contenuto di questa pagina</p>
          <a href="/" style="color: blue;">Menu Principale</a>
        </div>
      <% } else { %>
        <div class="row content">
          <div class="col-sm-2 sidenav" style="font-family: sans-serif;">
            <div style="text-align: center;">
              <br>
              <p><a href="ListaPazienti" style="text-decoration: none;"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;Lista pazienti</a></p>
              <p><a href="NuovoEsame" style="color: blue; text-decoration: none;"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Prescrivi esame</a></p>     <!-- DA FARE -->
              <p><a href="NuovaVisita" style="text-decoration: none;"><span class="glyphicon glyphicon-calendar"></span>&nbsp;&nbsp;Crea Visita</a></p> 
              <p><a href="NuovaRicetta" style="text-decoration: none;"><span class="glyphicon glyphicon-duplicate"></span>&nbsp;&nbsp;Crea Ricetta</a></p>
              <hr>  
              <p><a href="Opzioni" style="text-decoration: none;"><span class="glyphicon glyphicon-cog"></span>&nbsp;&nbsp;Opzioni</a></p>
            </div>
          </div>
          <div class="col-sm-10 text-left contentView"> 
            <h1><%= session.getAttribute("cognome")%> <%= session.getAttribute("nome")%></h1>
            <p style="padding-left: 10px;">
              <span style="color: blue;">Mail</span>&nbsp;&nbsp;<%= session.getAttribute("username")%><br>
              <span style="color: blue;">Matr</span>&nbsp;<%=session.getAttribute("matricola")%>
            </p>
            <hr>
            <p style="padding-left: 10px;">
              <span style="color: blue;">Prov</span>&nbsp;&nbsp;&nbsp;<%= session.getAttribute("provincia")%><br>
              <span style="color: blue;">Ruolo</span>&nbsp;<%= session.getAttribute("ruolo")%><br>
            </p>
            <hr><br>
            <h3>Nuovo Esame</h3><br>
            <form method="POST" action="NuovoEsame" name="FormEsame">
              <select class="form-control" id="sel1" style="width: 250px;" name="paziente" required>
                <option hidden disabled selected value>seleziona paziente</option>
                <%
                try {
                  Database db2 = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
                  Connection conn2 = db2.getConnection();
                  PreparedStatement st2 = conn2.prepareStatement("SELECT email, codice_fiscale FROM Pazienti WHERE id_medico=?");
                  st2.setString(1, (String) session.getAttribute("username"));
                  ResultSet rs2 = st2.executeQuery();
                  while(rs2.next()) {
                    if(request.getParameter("paziente")!=null && (request.getParameter("paziente").equals(rs2.getString(1)) || request.getParameter("paziente").equals(rs2.getString(2)))) {
                %>
                      <option value="<%= rs2.getString(1)%>" selected><%= rs2.getString(2)%></option>
                <%      
                    } else {
                %>
                      <option value="<%= rs2.getString(1)%>"><%= rs2.getString(2)%></option>
                <%
                  }}}catch(Exception e) {
                    response.sendRedirect("/");
                  }
                %>
              </select><br>
              <select class="form-control" required style="max-width: 250px; text-align: center;" name="esame">
                <option hidden disabled selected value>seleziona esame</option>
                <%
                  try {
                    Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
                    Connection conn = db.getConnection();
                    PreparedStatement st = conn.prepareStatement("SELECT id, nome FROM Esami");
                    ResultSet rs = st.executeQuery();
                    while(rs.next()) {
                %>
                      <option value="<%= rs.getInt(1)%>"><%= rs.getString(2)%></option>
                <%
                    }}catch(Exception e) {
                      response.sendRedirect("/");
                    }
                %>
              </select><br><br>
              <input type="submit" class="btn btn-primary" value="Conferma"><br>
            </form>
            <br><br><br>
          </div>
        </div>
      <% }
        }
        catch(Exception e) {
          response.sendRedirect("main.jsp");
        }
      %>   
    </div>
    
    <!--NAVBAR BOTTOM - standard per tutte le pagine-->
    <nav class="navbar navbar-inverse navbar-fixed-bottom">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">Servizi Sanitari</a>
        </div>
      </div>
    </nav>      

    <!--Script per creare degli array di appoggio utili alla suggestion box-->
    <script>
      var nomi = [];
      var cognomi = [];
      var email = [];
      var cf = [];
      var citta = [];
      <%
        try {
          Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
          Connection conn = db.getConnection();
          PreparedStatement st = conn.prepareStatement("SELECT nome, cognome, email, codice_fiscale, luogo_nascita FROM Pazienti WHERE id_medico = ?");
          st.setString(1, ((String) session.getAttribute("username")));
          ResultSet rs = st.executeQuery();
          while(rs.next()) {
      %>
        nomi.push("<%=rs.getString(1)%>");
        cognomi.push("<%=rs.getString(2)%>");
        email.push("<%=rs.getString(3)%>");
        cf.push("<%=rs.getString(4)%>");
        citta.push("<%=rs.getString(5)%>");
      <%
          }
        } catch(Exception e) {
          response.sendRedirect("/");
        }
      %>
      autocomplete(document.getElementById("myInput"), nomi,cognomi,email,cf,citta);
    </script>

  </body>
</html>