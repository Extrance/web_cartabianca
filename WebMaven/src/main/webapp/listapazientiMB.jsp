<%-- 
    Document   : listapazientiMB
    Created on : 5-ott-2019, 8.26.09
    Author     : andreabalasso
--%>
<%@page import="java.lang.String"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.MedicoB"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.MedicoBDAO"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.PazienteDAO"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.DAOCreate"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Paziente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Servizi sanitari - Lista Pazienti</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="icon" href="img/transparent.png" type="image/png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="css/sidenav.css" type="text/css"/>
    <link rel="stylesheet" href="css/autocompleteCss.css" type="text/css"/>
    <script type="text/javascript" src="js/autocompleteFile.js"></script>
    <style>
      body{
        background-image: url(img/background.jpg);
        background-position: top;
        background-attachment: fixed;
        background-repeat: no-repeat;
      }

      .btn-link:hover {
        opacity: 0.5;
      }
      @media screen and (min-width: 1050px) {
        .customInline {
           display: inline-block;
        }
      }
    </style>
  </head>
  <body>
  
    <% if(session.getAttribute("ruolo")!=null && session.getAttribute("ruolo").equals("Medico di Base")) { %>
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="img/transparent.png" width="40px" style="margin-top: -9.5px; "></a>
          </div>
          <div class="collapse navbar-collapse"  style="padding-left:30px;" id="myNavbar">
            <ul class="nav navbar-nav">
              <li><a href="/#section1">Chi siamo</a></li>
              <li><a href="/#section2">Servizi</a></li>
              <li><a href="/#section3">Dove trovarci</a></li>
              <li>
                <form class="navbar-form navbar-left" autocomplete="off" style="max-width: 250px;" action="RefertoPaziente" method="POST">
                  <div class="input-group autocomplete">
                    <input id="myInput" type="text" class="form-control" placeholder="Cerca paziente" name="paziente">
                    <div class="input-group-btn">
                      <button class="btn btn-default" type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </li>
            </ul>
            <div class="nav navbar-nav navbar-right" style="padding-right:10px;">
              <ul class="nav navbar-nav navbar-right">
                <li>
                  <form action="MedicoBasePage" method="GET">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="Medico di Base">
                      <span class="glyphicon glyphicon-user"></span>
                    </button>
                  </form>  
                </li>
                <li>
                  <form action="Slog" method="POST">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="LogOut">
                      <span class="glyphicon glyphicon-log-out"></span> Logout
                    </button>
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    <% } %>
    
    <%  if(session.getAttribute("errorMessage")!=null) {
    %>
      <script>
        window.alert("Errore: "+"<%= session.getAttribute("errorMessage")%>");
      </script>
    <%
          session.setAttribute("errorMessage", null);
        }
    %>
    
    <div class="container-fluid text-center" style="margin-top:50px">  
      <%
        try {
          MedicoBDAO utd = DAOCreate.getMedicoBDAO();
          MedicoB req = utd.getUtenteByEmail((String) session.getAttribute("username"));
        
          if (req==null) {    //no match found
      %>
        <div class="col-sm-12 text-left"> 
          <h1>Attenzione: Login come medico di base non effettuato</h1>
          <p>Si prega di effettuare il login per accedere al contenuto di questa pagina</p>
          <a href="/">Menu Principale</a>
        </div>
      <% } else { %>
        <div class="row content">
          <div class="col-sm-2 sidenav" style="font-family: sans-serif;">
            <div style="text-align: center;">
              <br>
              <p><a href="ListaPazienti" style="color: blue; text-decoration: none;"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;Lista pazienti</a></p>
              <p><a href="NuovoEsame" style="text-decoration: none;"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Prescrivi esame</a></p>
              <p><a href="NuovaVisita" style="text-decoration: none;"><span class="glyphicon glyphicon-calendar"></span>&nbsp;&nbsp;Crea Visita</a></p> 
              <p><a href="NuovaRicetta" style="text-decoration: none;"><span class="glyphicon glyphicon-duplicate"></span>&nbsp;&nbsp;Crea Ricetta</a></p>
              <hr>  
              <p><a href="Opzioni" style="text-decoration: none;"><span class="glyphicon glyphicon-cog"></span>&nbsp;&nbsp;Opzioni</a></p>
            </div>
          </div>
          <div class="col-sm-10 text-left contentView"> 
            <h1><%= session.getAttribute("cognome")%> <%= session.getAttribute("nome")%></h1>
            <p style="padding-left: 10px;">
              <span style="color: blue;">Mail</span>&nbsp;&nbsp;<%= session.getAttribute("username")%><br>
              <span style="color: blue;">Matr</span>&nbsp;<%=session.getAttribute("matricola")%>
            </p>
            <hr>
            <p style="padding-left: 10px;">
              <span style="color: blue;">Prov</span>&nbsp;&nbsp;&nbsp;<%= session.getAttribute("provincia")%><br>
              <span style="color: blue;">Ruolo</span>&nbsp;<%= session.getAttribute("ruolo")%><br>
            </p>
            <hr>
            <table class="table table-striped" style="width: 90%;">
              <thead>
                <tr>
                  <th style="text-align: center;">Nominativo</th>
                  <th style="text-align: center;">Data nascita</th>
                  <th style="text-align: center;">Ultima V</th>
                  <th style="text-align: center;">Ultima R</th>
                  <th style="text-align: center;">Opzioni</th>
                </tr>
              </thead>
              <tbody style="text-align: center;">
                <%
                  String ultimaVisita = "";
                  String ultimaRicetta = "";
                  Database db = Database.getInstance();
                  Connection conn = db.getConnection();
                  
                  PreparedStatement st;
                  if(request.getSession().getAttribute("paziente")==null) {
                    st = conn.prepareStatement("SELECT nome, cognome, codice_fiscale, data_nascita, luogo_nascita, email FROM Pazienti WHERE id_medico =?");
                    st.setString(1, (String) session.getAttribute("username"));
                  } else {
                    String paz = "";
                    if(request.getSession().getAttribute("paziente")!=null)
                      paz=(String) request.getSession().getAttribute("paziente");
                    st = conn.prepareStatement("SELECT nome, cognome, codice_fiscale, data_nascita, luogo_nascita, email FROM Pazienti WHERE id_medico=? AND (nome LIKE '%"+paz+"%' OR cognome LIKE '%"+paz+"%' OR codice_fiscale LIKE '%"+paz+"%' OR luogo_nascita LIKE '%"+paz+"%')");
                    st.setString(1, (String) session.getAttribute("username"));
                  }
                  
                  ResultSet rs = st.executeQuery();
                  while(rs.next()) {
                    ultimaVisita = "";
                    ultimaRicetta = "";
                    String paz = rs.getString(6);
                    
                    Database db1 = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
                    Connection conn1 = db1.getConnection();
                    PreparedStatement st1 = conn1.prepareStatement("SELECT V.data FROM Visita AS V WHERE V.id_paziente = ? ORDER BY id DESC LIMIT 1");
                    st1.setString(1, paz);
                    ResultSet rs1 = st1.executeQuery();
                    if(rs1.next()) {
                      ultimaVisita = ""+ rs1.getDate(1);
                    }
                    
                    Database db2 = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
                    Connection conn2 = db2.getConnection();
                    PreparedStatement st2 = conn2.prepareStatement("SELECT R.data FROM RicTot AS R WHERE R.id_paziente= ? ORDER BY R.id DESC LIMIT 1");
                    st2.setString(1, paz);
                    ResultSet rs2 = st2.executeQuery();
                    if(rs2.next()) {
                      ultimaRicetta = ""+ rs2.getDate(1);
                    }
                %>
                    <tr>
                      <td>
                        <form method="GET" action="RefertoPaziente">
                          <button type="submit" class="btn-link" style="text-align: left; background-color: transparent; color: blue; text-decoration: none; padding: 2px; font-size: 16px;" name="paziente" value="<%= rs.getString(3)%>"><%= rs.getString(2)%> <%= rs.getString(1)%></button>
                        </form>
                      </td>
                      <td><%= rs.getDate(4)%></td>
                      <td><%= ultimaVisita%></td>
                      <td><%= ultimaRicetta%></td>
                      <td>
                        <form method="GET" action="NuovaVisita" class="customInline" style="margin-bottom: 2px;">
                          <button type="submit" class="btn-link" style="width: 60px; background-color: red; color: white; text-decoration: none; padding: 2px; font-size: 12px;" name="paziente" value="<%= rs.getString(3)%>">Visita</button>
                        </form>
                        <form method="GET" action="NuovoEsame" class="customInline" style="margin-bottom: 2px;">
                          <button type="submit" class="btn-link" style="width: 60px; background-color: red; color: white; text-decoration: none; padding: 2px; font-size: 12px;" name="paziente" value="<%= rs.getString(3)%>">Esame</button>
                        </form>
                        <form method="GET" action="NuovaRicetta" class="customInline" style="margin-bottom: 2px;">
                          <button type="submit" class="btn-link" style="width: 60px; background-color: red; color: white; text-decoration: none; padding: 2px; font-size: 12px;" name="paziente" value="<%= rs.getString(3)%>">Ricetta</button>
                        </form>
                      </td>
                    </tr>
                <% } %>
              </tbody>
            </table>
            <br><br><br>
          </div>
        </div>
      <% }
        }
        catch(Exception e) {
          response.sendRedirect("main.jsp");
        }
      %>   
    </div>
    
    <!--NAVBAR BOTTOM - standard per tutte le pagine-->
    <nav class="navbar navbar-inverse navbar-fixed-bottom">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">Servizi Sanitari</a>
        </div>
      </div>
    </nav>      

    <!--Script per creare degli array di appoggio utili alla suggestion box-->
    <script>
      var nomi = [];
      var cognomi = [];
      var email = [];
      var cf = [];
      var citta = [];
      <%
        try {
          Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
          Connection conn = db.getConnection();
          PreparedStatement st = conn.prepareStatement("SELECT P.nome, P.cognome, P.email, P.codice_fiscale, P.luogo_nascita FROM Pazienti AS P WHERE P.id_medico = ?");
          st.setString(1, ((String) session.getAttribute("username")));
          ResultSet rs = st.executeQuery();
          while(rs.next()) {
            String paz = rs.getString(3);
      %>
        nomi.push("<%=rs.getString(1)%>");
        cognomi.push("<%=rs.getString(2)%>");
        email.push("<%=rs.getString(3)%>");
        cf.push("<%=rs.getString(4)%>");
        citta.push("<%=rs.getString(5)%>");
      <%
          }
        } catch(Exception e) {
          response.sendRedirect("/");
        }
      %>
      autocomplete(document.getElementById("myInput"), nomi,cognomi,email,cf,citta);
    </script>

  </body>
</html>