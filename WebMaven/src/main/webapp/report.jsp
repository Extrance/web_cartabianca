<%-- 
    Document   : report
    Created on : 17-dic-2019, 11.43.09
    Author     : andreabalasso
--%>
<%@page import="java.sql.Date"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Admin"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.AdminDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.PazienteDAO"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.DAOCreate"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Paziente"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Servizi sanitari - Report</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel = "icon" href ="img/transparent.png" type = "image/png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="css/sidenav.css" type="text/css"/>
    <style>
      body{
        background-position: top;
        background-attachment: fixed;
        background-repeat: no-repeat;
      }
      
      .custElem {
        background-color: transparent;
        padding: 0px;
        margin: 0px;
        height: 23px;
        font-size: 15px;
      }
      
      .ric {
        color: firebrick;
      }
      
      .exa {
        color: teal;
      }
     
      .custLi {
         list-style: none;
         margin-left: -25px;
      }
    </style>
  </head>
  <body>
    <%  if(session.getAttribute("errorMessage")!=null) {
          session.setAttribute("errorMessage", null);
    %>
      <script>
        window.alert("Errore in fase di elaborazione della richiesta");
      </script>
    <% } %>
    
    <% if(session.getAttribute("ruolo")!=null && session.getAttribute("ruolo").equals("Admin")) { %>
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="img/transparent.png" width="40px" style="margin-top: -9.5px; "></a>
          </div>
          <div class="collapse navbar-collapse"  style="padding-left:30px;" id="myNavbar">
            <div class="nav navbar-nav navbar-right" style="padding-right:10px;">
              <ul class="nav navbar-nav navbar-right">
                <li>
                  <form action="AdminPage" method="GET">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="Admin">
                      <span class="glyphicon glyphicon-user"></span>
                    </button>
                  </form>
                </li>
                <li>
                  <form action="Slog" method="POST">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="LogOut">
                      <span class="glyphicon glyphicon-log-out"></span> Logout
                    </button>
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    <% } %>
    
    <div class="container-fluid text-center" style="margin-top:50px">  
      <%
        try {
          AdminDAO utd = DAOCreate.getAdminDAO();
          Admin req = utd.getUtenteByEmail((String) session.getAttribute("username"));
        
          if (req==null) {    //no match found
      %>
        <div class="col-sm-12 text-left"> 
          <h1>Attenzione: Login come amministratore non effettuato</h1>
          <p>Si prega di effettuare il login per accedere al contenuto di questa pagina</p>
          <a href="privateLogin.jsp" class="btn btn-link">Login</a>
        </div>
      <% } else { %>
        <div class="row content">
          <div class="col-sm-2 sidenav" style="font-family: sans-serif;">
            <div style="text-align: center;">
              <br>
              <p><a href="#" style="color: blue; text-decoration: none;"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Report</a></p>
            </div>
          </div>
          <div class="col-sm-10 text-left contentView">
            <br><br>
            <span style="color: blue; font-weight: bold; font-size: 20px;">Provincia</span><span style="font-size: 20px;">&nbsp;&nbsp;&nbsp;<%= session.getAttribute("provincia")%></span><br>
            <hr>
            <span style="font-weight: bold; font-size: 18px;">Opzioni di report</span><br><br>
            <div class="row">
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#ricette" style="color: firebrick;">Ricette</a></li>
                <li><a data-toggle="tab" href="#esami" style="color: teal;">Esami</a></li>
              </ul>
              <!--<form id="myDownloadServlet" action="DownloadServlet" method="post">
                <input type="hidden" id="fileName" name="fileName" value="pokedex.xlsx"/>
                <input type="submit" id="btnDownload" name="btnDownload" value="Download File" />
              </form>-->
              
              <div class="tab-content">
                <div id="ricette" class="tab-pane fade in active" style="margin-left: 10px;">
                  <div style="margin-left: 20px; margin-top: 20px;">
                    <form id="myDownloadRicette" action="Report" method="POST">
                      <button type="submit" class="btn-link ric" style="font-weight: bold;" name="btnDownload" value="Ricette"><img src="img/excel.png" style="width: 25px; height: 25px;"/> Tutte le ricette</button><br><br>  <!--OK-->
                      <div class="col-sm-6">
                        Ricette raggruppate per Farmaco<br>
                        <ul>
                          <li class="custLi"><button type="submit" class="btn-link custElem ric" name="btnDownload" value="RicFarAll"><img src="img/excel.png" style="width: 15px; height: 15px;"/> tutte</button></li>        <!--OK-->
                          <li class="custLi"><button type="submit" class="btn-link custElem ric" name="btnDownload" value="RicFarYea"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo anno</button></li>  <!--OK-->
                          <li class="custLi"><button type="submit" class="btn-link custElem ric" name="btnDownload" value="RicFarMon"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo mese</button></li>  <!--OK-->
                        </ul>
                        Ricette ordinate per Paziente<br>
                        <ul>
                          <li class="custLi"><button type="submit" class="btn-link custElem ric" name="btnDownload" value="RicPazAll"><img src="img/excel.png" style="width: 15px; height: 15px;"/> tutte</button></li>        <!--SQL ok-->
                          <li class="custLi"><button type="submit" class="btn-link custElem ric" name="btnDownload" value="RicPazYea"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo anno</button></li>  <!--SQL ok-->
                          <li class="custLi"><button type="submit" class="btn-link custElem ric" name="btnDownload" value="RicPazMon"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo mese</button></li>  <!--SQL ok-->
                        </ul>
                      </div>
                      <div class="col-sm-6">
                        Ricette ordinate per Data<br>
                        <ul>
                          <li class="custLi"><button type="submit" class="btn-link custElem ric" name="btnDownload" value="RicDatAll"><img src="img/excel.png" style="width: 15px; height: 15px;"/> tutte</button></li>        <!--SQL ok-->
                          <li class="custLi"><button type="submit" class="btn-link custElem ric" name="btnDownload" value="RicDatYea"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo anno</button></li>  <!--SQL ok-->
                          <li class="custLi"><button type="submit" class="btn-link custElem ric" name="btnDownload" value="RicDatMon"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo mese</button></li>  <!--SQL ok-->
                        </ul>
                        Ricette ordinate per M di Base<br>
                        <ul>
                          <li class="custLi"><button type="submit" class="btn-link custElem ric" name="btnDownload" value="RicMBAll"><img src="img/excel.png" style="width: 15px; height: 15px;"/> tutte</button></li>        <!--SQL ok-->
                          <li class="custLi"><button type="submit" class="btn-link custElem ric" name="btnDownload" value="RicMBYea"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo anno</button></li>  <!--SQL ok-->
                          <li class="custLi"><button type="submit" class="btn-link custElem ric" name="btnDownload" value="RicMBMon"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo mese</button></li>  <!--SQL ok-->
                        </ul>
                      </div>
                    </form>
                  </div>
                </div>
                
                <div id="esami" class="tab-pane fade" style="margin-left: 10px;">
                  <div style="margin-left: 20px; margin-top: 20px;">
                    <form id="myDownloadRicette" action="Report" method="POST">
                      <button type="submit" class="btn-link exa" style="font-weight: bold;" name="btnDownload" value="Esami"><img src="img/excel.png" style="width: 25px; height: 25px;"/> Tutte le prescrizioni</button><br><br>
                      <div class="col-sm-6">
                        Prescr. raggruppate per Esame<br>
                        <ul>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="PreExaAll"><img src="img/excel.png" style="width: 15px; height: 15px;"/> tutte</button></li>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="PreExaYea"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo anno</button></li>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="PreExaMon"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo mese</button></li>
                        </ul>
                        Prescr. ordinate per Paziente<br>
                        <ul>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="PrePazAll"><img src="img/excel.png" style="width: 15px; height: 15px;"/> tutte</button></li>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="PrePazYea"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo anno</button></li>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="PrePazMon"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo mese</button></li>
                        </ul>
                        Prescr. ordinate per Data<br>
                        <ul>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="PreDatAll"><img src="img/excel.png" style="width: 15px; height: 15px;"/> tutte</button></li>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="PreDatYea"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo anno</button></li>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="PreDatMon"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo mese</button></li>
                        </ul>
                        Prescr. ordinate per M di Base<br>
                        <ul>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="PreMBAll"><img src="img/excel.png" style="width: 15px; height: 15px;"/> tutte</button></li>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="PreMBYea"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo anno</button></li>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="PreMBMon"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo mese</button></li>
                        </ul>
                      </div>
                      <div class="col-sm-6">
                        Esami erogati ordinati per M Spec<br>
                        <ul>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="ExaMSAll"><img src="img/excel.png" style="width: 15px; height: 15px;"/> tutte</button></li>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="ExaMSYea"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo anno</button></li>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="ExaMSMon"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo mese</button></li>
                        </ul>
                        Esami erogati ordinati per Data<br>
                        <ul>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="ExaDatAll"><img src="img/excel.png" style="width: 15px; height: 15px;"/> tutte</button></li>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="ExaDatYea"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo anno</button></li>
                          <li class="custLi"><button type="submit" class="btn-link custElem exa" name="btnDownload" value="ExaDatMon"><img src="img/excel.png" style="width: 15px; height: 15px;"/> ultimo mese</button></li>
                        </ul>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br><br><br>
      <% }
        }
        catch(Exception e) {
          response.sendRedirect("privateLogin.jsp");
        }
      %>   
    </div>
    
    <nav class="navbar navbar-inverse navbar-fixed-bottom">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Servizi Sanitari</a>
        </div>
      </div>
    </nav>      

  </body>
</html>