<%-- 
    Document   : adminpage
    Created on : 16-dic-2019, 11.16.12
    Author     : andreabalasso
--%>
<%@page import="java.sql.Date"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Admin"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.AdminDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.PazienteDAO"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.DAOCreate"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Paziente"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Servizi sanitari - Admin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel = "icon" href ="img/transparent.png" type = "image/png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    
    <link rel="stylesheet" href="css/sidenav.css" type="text/css"/>
    <style>
      body{
        background-position: top;
        background-attachment: fixed;
        background-repeat: no-repeat;
      }
    </style>
  </head>
  <body>
    
    <% if(session.getAttribute("ruolo")!=null && session.getAttribute("ruolo").equals("Admin")) { %>
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="img/transparent.png" width="40px" style="margin-top: -9.5px; "></a>
          </div>
          <div class="collapse navbar-collapse"  style="padding-left:30px;" id="myNavbar">
            <div class="nav navbar-nav navbar-right" style="padding-right:10px;">
              <ul class="nav navbar-nav navbar-right">
                <li>
                  <form action="AdminPage" method="GET">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="Admin">
                      <span class="glyphicon glyphicon-user"></span>
                    </button>
                  </form>
                </li>
                <li>
                  <form action="Slog" method="POST">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="LogOut">
                      <span class="glyphicon glyphicon-log-out"></span> Logout
                    </button>
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    <% } %>
    
    <div class="container-fluid text-center" style="margin-top:50px">  
      <%
        try {
          AdminDAO utd = DAOCreate.getAdminDAO();
          Admin req = utd.getUtenteByEmail((String) session.getAttribute("username"));
        
          if (req==null) {    //no match found
      %>
        <div class="col-sm-12 text-left"> 
          <h1>Attenzione: Login come amministratore non effettuato</h1>
          <p>Si prega di effettuare il login per accedere al contenuto di questa pagina</p>
          <a href="privateLogin.jsp" class="btn btn-link">Login</a>
        </div>
      <% } else { %>
          
        <div class="row content">
          <div class="col-sm-2 sidenav" style="font-family: sans-serif;">
            <div style="text-align: center;">
              <br>
              <p><a href="Report" style="text-decoration: none;"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Report</a></p>
            </div>
          </div>
          <div class="col-sm-10 text-left contentView">
            <br><br>
            <span style="color: blue; font-weight: bold; font-size: 20px;">Provincia</span><span style="font-size: 20px;">&nbsp;&nbsp;&nbsp;<%= req.getProvincia()%></span><br><br>
            Di seguito l'elenco delle ricette e degli esami prescritti in data odierna per la <span style="font-weight: bold;">provincia <%= req.getProvincia()%></span>.<br>
            Si faccia riferimento alla sezione <a href="Report" style="color: blue;">Report</a> per ulteriori dati. 
            <br><hr><br>
            <div class="row">
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#ricette" style="color: firebrick;">Ricette</a></li>
                <li><a data-toggle="tab" href="#esami" style="color: teal;">Esami</a></li>
              </ul>
              <br>
              <div class="tab-content">
                <div id="ricette" class="tab-pane fade in active" style="margin-left: 10px;">
                  <span style="font-weight: bold; color: firebrick;">Ricette prescritte in data odierna</span><br>
                    <table class="table table-bordered table-striped" style="width: 90%; margin-top: 10px;">
                      <thead style="background-color: firebrick;">
                        <tr>
                          <th style="text-align: center;">ID</th>
                          <th style="text-align: center;">Farmaco</th>
                          <th style="text-align: center;">Qta'</th>
                          <th style="text-align: center;">Medico B</th>
                          <th style="text-align: center;">Paziente</th>
                          <th style="text-align: center;">Data</th>
                          <th style="text-align: center;">Status</th>
                        </tr>
                      </thead>
                      <tbody id="myTable" style="text-align: center;">
                        <%
                          java.util.Date utilDate = new java.util.Date();
                          java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                          Date data = sqlDate;

                          Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
                          Connection conn = db.getConnection();
                          PreparedStatement st = conn.prepareStatement("SELECT R.id, R2.id_farmaco, F.nome, R2.quantita, M.matricola, P.codice_fiscale, R.data, R.erogata FROM RicTot AS R, Ricette2 AS R2, MediciB AS M, Farmaci AS F, Pazienti AS P WHERE R.id=R2.id_ric AND R2.id_farmaco = F.id AND R.id_medico = M.email AND R.id_paziente = P.email AND M.provincia = ? AND R.data = ?");
                          st.setString(1, req.getProvincia());
                          st.setDate(2,data);

                          ResultSet rs = st.executeQuery();
                          while(rs.next()) {
                        %>
                          <tr>
                            <td><%= rs.getInt(1)%></td>
                            <td><%= rs.getInt(2)%>-<%= rs.getString(3)%></td>
                            <td><%= rs.getInt(4)%></td>
                            <td><%= rs.getString(5)%></td>
                            <td><%= rs.getString(6)%></td>
                            <td><%= rs.getDate(7)%></td>
                            <td>
                              <%if(rs.getBoolean(8)) {%>
                                <span style="color: green">erogata</span>
                              <% } else {%>
                                <span style="color: red">da erogare</span>
                              <% } %>
                            </td>
                          </tr>
                        <%  
                          }
                        %>
                      </tbody>
                    </table>
                  </div>
                
                  <div id="esami" class="tab-pane fade" style="margin-left: 10px;">
                  <span style="font-weight: bold; color: teal;">Esami prescritti o erogati in data odierna</span><br>
                    <table class="table table-bordered table-striped" style="width: 90%; margin-top: 10px;">
                      <thead style="background-color: teal;">
                        <tr>
                          <th style="text-align: center;">ID</th>
                          <th style="text-align: center;">Esame</th>
                          <th style="text-align: center;">Medico</th>
                          <th style="text-align: center;">Paziente</th>
                          <th style="text-align: center;">Status</th>
                          <th style="text-align: center;">Ticket</th>
                          <th style="text-align: center;">Prescritto</th>
                          <th style="text-align: center;">Erogato</th>
                        </tr>
                      </thead>
                      <tbody id="myTable" style="text-align: center;">
                        <%
                          PreparedStatement st2 = conn.prepareStatement("SELECT Pr.id, Pr.id_esame, E.nome, P.codice_fiscale, M.matricola, Pr.erogato, Pr.ticket, Pr.dataV, Pr.data FROM MediciB AS M, Prescrizioni AS Pr, Pazienti AS P, Esami AS E WHERE M.email=Pr.id_medicoB AND Pr.id_paziente=P.email AND Pr.id_esame = E.id AND P.id_provincia =? AND Pr.erogato=false AND Pr.dataV=? UNION SELECT Pr.id, Pr.id_esame, E.nome, P.codice_fiscale, M.matricola, Pr.erogato, Pr.ticket, Pr.dataV, Pr.data FROM Prescrizioni AS Pr, Pazienti AS P, Esami AS E, MediciS AS M WHERE Pr.id_paziente = P.email AND Pr.id_medicoS = M.email AND Pr.id_esame = E.id AND P.id_provincia =? AND Pr.erogato=true AND (Pr.data=? OR Pr.dataV=?)");
                          st2.setString(1, req.getProvincia());
                          st2.setDate(2,data);
                          st2.setString(3, req.getProvincia());
                          st2.setDate(4,data);
                          st2.setDate(5,data);

                          ResultSet rs2 = st2.executeQuery();
                          while(rs2.next()) {
                        %>
                          <tr>
                            <td><%= rs2.getInt(1)%></td>
                            <td><%= rs2.getInt(2)%>-<%= rs2.getString(3)%></td>
                            <td>
                              <%if(rs2.getBoolean(6)) {%>
                                <span style="color: green">MS-</span><%= rs2.getString(5)%>
                              <% } else {%>
                                <span style="color: red">MB-</span><%= rs2.getString(5)%>
                              <% } %>
                            </td>
                            <td><%= rs2.getString(4)%></td>
                            <td>
                              <%if(rs2.getBoolean(6)) {%>
                                <span style="color: green">erogato</span>
                              <% } else {%>
                                <span style="color: red">da erogare</span>
                              <% } %>
                            </td>
                            <td>
                              <%
                                if(rs2.getBoolean(6)) {
                                  if(rs2.getBoolean(7)) {
                              %>
                                <span style="color: green">pagato</span>
                              <% } else { %>
                                <span style="color: red">da pagare</span>
                              <% } 
                                }
                              %>
                            </td>
                            <td><%= rs2.getDate(8)%></td>
                            <td>
                              <%
                                if(rs2.getDate(9)!=null) {
                              %>
                                <%= rs2.getDate(9)%>
                              <% }%>
                            </td>
                          </tr>
                        <%  
                          }
                        %>
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <br><br><br>
      <% }
        }
        catch(Exception e) {
          response.sendRedirect("privateLogin.jsp");
        }
      %>   
    </div>
    
    <nav class="navbar navbar-inverse navbar-fixed-bottom">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Servizi Sanitari</a>
        </div>
      </div>
    </nav>      

  </body>
</html>