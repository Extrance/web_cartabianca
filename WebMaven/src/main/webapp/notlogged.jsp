<%-- 
    Document   : notlogged
    Created on : 5-ott-2019, 9.55.43
    Author     : andreabalasso
--%>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Servizi sanitari - Cambia Password</title>
    <link rel="icon" href="img/transparent.png" type="image/png">
    <link rel="stylesheet" href="css/topnav.css">
    <link rel="stylesheet" href="css/page.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="css/topnav.css" type="text/css"/>
    <style>
      body{
        background-image: url(img/background.jpg);
        background-position: top;
        background-attachment: fixed;
        background-repeat: no-repeat;
      }
    </style>
  </head>
  <body>
    <div class="page">
      <nav id="page">
        <br><br>
        <h2>Si prega di effettuare il login specifico per accedere alla sezione desiderata</h2>
        <form action="LoginPaziente" method="GET">
            <button type="submit" formaction="LoginPaziente">Login Paziente</button>
            <button type="submit" formaction="LoginMedicoBase">Login Medico di Base</button>
            <button type="submit" formaction="LoginMedicoSpecialista">Login Medico Specialista</button>
        </form>
      </nav>
      <br><br><br>
    </div>
  </body>
</html>