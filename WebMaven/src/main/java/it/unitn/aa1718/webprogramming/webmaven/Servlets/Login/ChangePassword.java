package it.unitn.aa1718.webprogramming.webmaven.Servlets.Login;

import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.DAOCreate;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.MedicoBDAO;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.MedicoSDAO;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.PazienteDAO;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.MedicoB;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.MedicoS;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Paziente;
import it.unitn.aa1718.webprogramming.webmaven.SendMail;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.mail.EmailException;
/**
 *
 * @author andreabalasso
 */
public class ChangePassword extends HttpServlet{
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();  
    if(session.getAttribute("ruolo")==null)
      request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
    else
      response.sendRedirect("Opzioni");
  }
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws java.io.IOException
   * @throws javax.servlet.ServletException
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {    
    
    HttpSession session = request.getSession();
    if(session.getAttribute("username") == null) {
      response.setStatus(401); //Dovrebbe bastare questo per non avere problemi con utenti già loggati che accedono in post alla servlet
    }
        
    //inizializzo le variabili fornite dal form richiedente
    String email = request.getParameter("username");
    String password = request.getParameter("pw");
    String newPw1 = request.getParameter("newPw1");
    String newPw2 = request.getParameter("newPw2");
        
    //handling per il Paziente
    if(session.getAttribute("ruolo").equals("Paziente")) {
      PazienteDAO utd = DAOCreate.getUtenteDAO();
      try {
        if(utd.isUsed(email)){
          Paziente currentuser = utd.getUtenteByEmail(email);
          if(password.equals(currentuser.getPassword())) {      
            if(newPw1.equals(newPw2)) {
              Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
              Connection conn = db.getConnection();
              PreparedStatement st = conn.prepareStatement("update Pazienti set Pazienti.password=? where Pazienti.email = ?");
              st.setString(1, newPw1);
              st.setString(2, email);
              int rs = st.executeUpdate();
              //MANDA MAIL
              SendMail.changePasswordMail(email,newPw1);
              
              response.sendRedirect("/");
            } else {
              session.setAttribute("errorMessage", "Errore nella conferma della nuova password");
              response.sendRedirect("Opzioni");
            }
          } else {
            session.setAttribute("errorMessage", "La combinazione email/password è sbagliata");
            response.sendRedirect("Opzioni");
          }
        } else {
          session.setAttribute("errorMessage", "L'indirizzo mail non è corretto");
          response.sendRedirect("Opzioni");
        }  
      } catch (SQLException | EmailException ex) {
        session.setAttribute("errorMessage", "Errore");
        response.sendRedirect("Opzioni");
      }
    }
    //handling per il Medico di Base
    else if(session.getAttribute("ruolo").equals("Medico di Base")) {
      MedicoBDAO utd = DAOCreate.getMedicoBDAO();
      try {
        if(utd.isUsed(email)){
          MedicoB currentuser = utd.getUtenteByEmail(email);
          if(password.equals(currentuser.getPassword())) {
            if(newPw1.equals(newPw2)) {
              Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
              Connection conn = db.getConnection();
              PreparedStatement st = conn.prepareStatement("update MediciB set MediciB.password=? where MediciB.email = ?");
              st.setString(1, newPw1);
              st.setString(2, email);
              int rs = st.executeUpdate();
              //MANDA MAIL
              SendMail.changePasswordMail(email,newPw1);
              
              response.sendRedirect("/");
            } else {
              session.setAttribute("errorMessage", "Errore nella conferma della nuova password");
              
              response.sendRedirect("Opzioni");
            }
          } else {
            session.setAttribute("errorMessage", "La combinazione email/password è sbagliata");
            response.sendRedirect("Opzioni");
          }
        } else {
          session.setAttribute("errorMessage", "L'indirizzo mail non è corretto");
          response.sendRedirect("Opzioni");
        }  
      } catch (SQLException | EmailException ex) {
        session.setAttribute("errorMessage", "Errore");
        response.sendRedirect("Opzioni");
      }
    }
    //handling del Medico Specialista
    else if(session.getAttribute("ruolo").equals("Medico Specialista")) {
      MedicoSDAO utd = DAOCreate.getMedicoSDAO();
      try {
        if(utd.isUsed(email)){
          MedicoS currentuser = utd.getUtenteByEmail(email);
          if (password.equals(currentuser.getPassword())) {
            if(newPw1.equals(newPw2)) {
              Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
              Connection conn = db.getConnection();
              PreparedStatement st = conn.prepareStatement("update MediciS set MediciS.password=? where MediciS.email = ?");
              st.setString(1, newPw1);
              st.setString(2, email);
              int rs = st.executeUpdate();
              //MANDA MAIL
              SendMail.changePasswordMail(email,newPw1);
                        
              response.sendRedirect("/");
            } else {
              session.setAttribute("errorMessage", "Errore nella conferma della nuova password");
              response.sendRedirect("Opzioni");
            }
          } else {
            session.setAttribute("errorMessage", "La combinazione email/password è sbagliata");
            response.sendRedirect("Opzioni");
          }
        } else {
          session.setAttribute("errorMessage", "L'indirizzo mail non è corretto");
          response.sendRedirect("Opzioni");
        }  
      } catch (SQLException | EmailException ex) {
        session.setAttribute("errorMessage", "Errore");
        response.sendRedirect("Opzioni");
      }
    }
    //ruolo non riconosciuto
    else {
      request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
    }
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Servlet che permette all'Utente di cambiare la propria password";
  }// </editor-fold>
}