package it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO;

import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Admin;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 *
 * @author andreabalasso
 */
public class AdminDAO extends User{
  Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
  Connection conn = db.getConnection();
  
  public boolean isUsed(String idOperatore) throws SQLException {    //Funzione comoda per capire se esiste già un record 'username'
    Boolean result;

    if (idOperatore != null) {
      PreparedStatement st = conn.prepareStatement("SELECT true FROM Admin WHERE id = ?");
      st.setString(1, idOperatore);

      ResultSet rs = st.executeQuery();
      if (rs.next()) {
        result = true;
      } else {
        result = false;
      }
    } else {
      result = false;
    }
    return result;
  }
  
  public Admin getUtenteByEmail(String idOperatore) throws SQLException {
    Admin result;

    if (idOperatore == null)
      return null;
    
    PreparedStatement st = conn.prepareStatement("select * from Admin where Admin.id = ?");
    st.setString(1, idOperatore);
    ResultSet rs = st.executeQuery();
    if (rs.next()) {
      result = new Admin();
      result.setId(rs.getString(1));
      result.setPassword(rs.getString(2));
      result.setProvincia(rs.getString(3));

      if (rs.next()) {
        throw new SQLException("Entry duplicata per la stessa email");
      }
    } else {
      result = null;
    }
    return result;
  }
}
