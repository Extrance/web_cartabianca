package it.unitn.aa1718.webprogramming.webmaven.Servlets;

import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 *
 * @author andreabalasso
 */
@MultipartConfig
public class UploadImage extends HttpServlet{
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
  */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
      HttpSession session = request.getSession();
      if(session.getAttribute("ruolo")==null || !session.getAttribute("ruolo").equals("Paziente"))
        request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
      else
        request.getRequestDispatcher("upload.jsp").forward(request, response);
    }
  }
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();
    if(session.getAttribute("username")==null || !session.getAttribute("ruolo").equals("Paziente"))
      response.setStatus(400);
    else {
      String pathAppoggio = this.getClass().getClassLoader().getResource("").getPath();
      String fullPath = URLDecoder.decode(pathAppoggio, "UTF-8");
      String pathArr[] = fullPath.split("target/");
      
      fullPath = pathArr[0];
      fullPath = fullPath + "src/main/webapp/img/users/";
      
      String savedFileName = fullPath + session.getAttribute("cf") +".jpg";

      File fileToSave = new File(savedFileName);
      fileToSave.getParentFile().mkdirs();
      fileToSave.delete();
      //Generate path file to copy file
      Path folder = Paths.get(savedFileName);
      Path fileToSavePath = Files.createFile(folder);
      //Copy file to server
      InputStream input = request.getPart("myImage").getInputStream();
      Files.copy(input, fileToSavePath, StandardCopyOption.REPLACE_EXISTING);
      
      try {
        Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
        Connection conn = db.getConnection();
        PreparedStatement st;
        st = conn.prepareStatement("UPDATE Pazienti SET foto=? WHERE email=?");
        st.setString(1, (String) session.getAttribute("cf"));
        st.setString(2, (String) session.getAttribute("username"));
        int rs = st.executeUpdate();
      } catch (SQLException ex) {
        session.setAttribute("errorMessage","Errore accesso al DB");
        response.sendRedirect("PazientePage");
      }
      response.sendRedirect("PazientePage");
    }
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Servlet che permette all'Utente di caricare la propria immagine";
  }// </editor-fold>
}