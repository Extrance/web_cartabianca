package it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO;

import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.MedicoS;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 *
 * @author andreabalasso
 */
public class MedicoSDAO extends User{
  Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
  Connection conn = db.getConnection();

  public boolean isUsed(String email) throws SQLException {    //Funzione comoda per capire se esiste già un record 'username'
    Boolean result;

    if (email != null) {
        PreparedStatement st = conn.prepareStatement("SELECT true FROM MediciS WHERE email = ?");
        st.setString(1, email);

        ResultSet rs = st.executeQuery();
        if (rs.next()) {
            result = true;
        } else {
            result = false;
        }
    } else {
        result = false;
    }
    return result;
  }

  public MedicoS getUtenteByEmail(String email) throws SQLException {
    MedicoS result;

    if (email == null) {
        return null;
    }
    PreparedStatement st = conn.prepareStatement("select * from MediciS where MediciS.email = ?");
    st.setString(1, email);
    ResultSet rs = st.executeQuery();
    if (rs.next()) {
      result = new MedicoS();
      result.setEmail(rs.getString(1));
      result.setPassword(rs.getString(2));
      result.setNome(rs.getString(3));
      result.setCognome(rs.getString(4));
      result.setCitta(rs.getString(5));
      result.setProvincia(rs.getString(6));
      result.setMatricola(rs.getString(7));

      if (rs.next()) {
          throw new SQLException("Entry duplicata per la stessa email");
      }
    } else {
        result = null;
    }
    return result;
  }
}