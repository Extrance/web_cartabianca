package it.unitn.aa1718.webprogramming.webmaven.Servlets.Login;

import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.DAOCreate;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.MedicoBDAO;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.MedicoSDAO;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.PazienteDAO;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.MedicoB;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.MedicoS;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Paziente;
import it.unitn.aa1718.webprogramming.webmaven.GenerateSecurePassword;
import it.unitn.aa1718.webprogramming.webmaven.SendMail;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.mail.EmailException;
/**
 *
 * @author andreabalasso
 */
public class ForgotPassword extends HttpServlet{
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    HttpSession session = request.getSession();
    try (PrintWriter out = response.getWriter()) {
      if(session.getAttribute("ruolo")==null)
        request.getRequestDispatcher("forgot.jsp").forward(request, response);
      else
        response.sendRedirect("/");
    }
  }
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws java.io.IOException
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {        
    HttpSession session = request.getSession();
    if (session.getAttribute("username") != null) {
      response.setStatus(500);//Commento//Dovrebbe bastare questo per non avere problemi con utenti già loggati che accedono in post alla servlet
    }
        
    String email = request.getParameter("username");
    String cf = request.getParameter("cf");
    String ruolo = request.getParameter("ruolo");
        
    switch (ruolo) {
      case "paziente":
        PazienteDAO utdP = DAOCreate.getUtenteDAO();
        try {
          if(utdP.isUsed(email)){
            Paziente currentuser = utdP.getUtenteByEmail(email);
            if (cf.equals(currentuser.getCf())) {
              Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
              Connection conn = db.getConnection();
                
              String newPw = GenerateSecurePassword.generatePassword(16);
                
              PreparedStatement st = conn.prepareStatement("update Pazienti set Pazienti.password=? where Pazienti.email = ?");
              st.setString(1, newPw);
              st.setString(2, email);
              int rs = st.executeUpdate();
              //MANDA MAIL
              SendMail.resetPasswordMail(email,newPw);
              response.sendRedirect("/");
            } else {
              session.setAttribute("errorMessage", "La combinazione email/codice fiscale è sbagliata");
              response.sendRedirect("/");
            }
          } else {
            session.setAttribute("errorMessage", "L'indirizzo mail non è corretto");
            response.sendRedirect("/");
          }  
        } catch (SQLException | EmailException ex) {
          session.setAttribute("errorMessage", "Errore");
          response.sendRedirect("/");
        }
        break;
      case "medicoB":
        MedicoBDAO utdMB = DAOCreate.getMedicoBDAO();
        try {
          if(utdMB.isUsed(email)){
            MedicoB currentuser = utdMB.getUtenteByEmail(email);
            if (cf.equals(currentuser.getMatricola())) {
              Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
              Connection conn = db.getConnection();
                  
              String newPw = GenerateSecurePassword.generatePassword(16);
                   
              PreparedStatement st = conn.prepareStatement("update MediciB set MediciB.password=? where MediciB.email = ?");
              st.setString(1, newPw);
              st.setString(2, email);
              int rs = st.executeUpdate();
              //MANDA MAIL
              SendMail.resetPasswordMail(email,newPw);
              response.sendRedirect("/");
            } else {
              session.setAttribute("errorMessage", "La combinazione email/codice fiscale è sbagliata");
              response.sendRedirect("/");
            }
          } else {
            session.setAttribute("errorMessage", "L'indirizzo mail non è corretto");
            response.sendRedirect("/");
          }  
        } catch (SQLException | EmailException ex) {
          session.setAttribute("errorMessage", "Errore");
          response.sendRedirect("/");
        }
        break;
      case "medicoS":
        MedicoSDAO utdMS = DAOCreate.getMedicoSDAO();
        try {
          if(utdMS.isUsed(email)){
            MedicoS currentuser = utdMS.getUtenteByEmail(email);
            if (cf.equals(currentuser.getMatricola())) {
              Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
              Connection conn = db.getConnection();
                    
              String newPw = GenerateSecurePassword.generatePassword(16);
                    
              PreparedStatement st = conn.prepareStatement("update MediciS set MediciS.password=? where MediciS.email = ?");
              st.setString(1, newPw);
              st.setString(2, email);
              int rs = st.executeUpdate();
              //MANDA MAIL
              SendMail.resetPasswordMail(email,newPw);
              response.sendRedirect("/");
            } else {
              session.setAttribute("errorMessage", "La combinazione email/codice fiscale è sbagliata");
              response.sendRedirect("/");
            }
          } else {
            session.setAttribute("errorMessage", "L'indirizzo mail non è corretto");
            response.sendRedirect("/");
          }  
        } catch (SQLException | EmailException ex) {
          Logger.getLogger(ForgotPassword.class.getName()).log(Level.SEVERE, null, ex);
        }
        break;
      default:
        session.setAttribute("errorMessage", "Errore inaspettato");
        response.sendRedirect("/");
        break;
    }
  }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet che permette all'Utente di resettare la propria password";
    }// </editor-fold>
}