package it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities;
/**
 *
 * @author andreabalasso
 */
public class Paziente {
  private String email;
  private String password;
  private String nome;
  private String cognome;
  private String sesso;
  private String data_nascita;
  private String luogo_nascita;
  private String cf;
  private String foto;
  private String id_medico;
  private String provincia;

  public Paziente() { //Il costruttore standard può essere tranquillamente vuoto
  }

  public Paziente(String email, String password, String nome, String cognome, String sesso, String data_nascita, String luogo_nascita, String cf, String foto, String id_medico, String provincia) {
    this.email = email;
    this.password = password;
    this.nome = nome;
    this.cognome = cognome;
    this.sesso = sesso;
    this.data_nascita = data_nascita;
    this.luogo_nascita = luogo_nascita;
    this.cf = cf;
    this.foto = foto;
    this.id_medico = id_medico;
    this.provincia = provincia;
  }

  @Override
  public String toString() {
      return "Paziente[" + getEmail() + "," + getNome() + "," + getCognome() + "," + getSesso() + "," + getDataNascita() + "," + getLuogoNascita() + "," + getCf() + "," + getFoto() + "," + getMedico() + "," + getProvincia() +"]";
  }
  /**
   * @return the email
   */
  public String getEmail() {
      return email;
  }
  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
      this.email = email;
  }
  /**
   * @return the password
   */
  public String getPassword() {
      return password;
  }
  /**
   * @param password the password to set
   */
  public void setPassword(String password) {
      this.password = password;
  }
  /**
   * @return the nome
   */
  public String getNome() {
      return nome;
  }
  /**
   * @param nome the nome to set
   */
  public void setNome(String nome) {
      this.nome = nome;
  }
  /**
   * @return the cognome
   */
  public String getCognome() {
      return cognome;
  }
  /**
   * @param cognome the cognome to set
   */
  public void setCognome(String cognome) {
      this.cognome = cognome;
  }
  /**
   * @return the sesso
   */
  public String getSesso() {
      return sesso;
  }
  /**
   * @param sesso the sesso to set
   */
  public void setSesso(String sesso) {
      this.sesso = sesso;
  }
  /**
   * @return the data di nascita
   */
  public String getDataNascita() {
      return data_nascita;
  }
  /**
   * @param data_nascita the data di nascita to set
   */
  public void setDataNascita(String data_nascita) {
      this.data_nascita = data_nascita;
  }
  /**
   * @return the luogo di nascita
   */
  public String getLuogoNascita() {
      return luogo_nascita;
  }
  /**
   * @param luogo_nascita the luogo di nascita to set
   */
  public void setLuogoNascita(String luogo_nascita) {
      this.luogo_nascita = luogo_nascita;
  }
  /**
   * @return the cf
   */
  public String getCf() {
      return cf;
  }
  /**
   * @param cf the cf to set
   */
  public void setCf(String cf) {
      this.cf = cf;
  }
  /**
   * @return the photo
   */
  public String getFoto() {
      return foto;
  }/**
   * @param foto the foto to set
   */
  public void setFoto(String foto) {
      this.foto = foto;
  }
  /**
   * @return the photo
   */
  public String getMedico() {
      return id_medico;
  }
  /**
   * @param id_medico the medico to set
   */
  public void setMedico(String id_medico) {
      this.id_medico = id_medico;
  }
  /**
   * @return the provincia
   */
  public String getProvincia() {
      return provincia;
  }
  /**
   * @param provincia the provincia to set
   */
  public void setProvincia(String provincia) {
      this.provincia = provincia;
  }
}