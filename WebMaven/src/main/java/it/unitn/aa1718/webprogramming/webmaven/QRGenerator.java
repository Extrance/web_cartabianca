package it.unitn.aa1718.webprogramming.webmaven;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
/**
 *
 * @author andreabalasso
 */
public class QRGenerator {
  
  public static int WIDTH = 200;
  public static int HEIGHT = 200;
  
  public static void generateQRCodeImage(String text, int ricettaId, String parameterPath) throws WriterException, IOException {
    QRCodeWriter qrCodeWriter = new QRCodeWriter();
    BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, WIDTH, HEIGHT);

    String chosenPath = parameterPath + "QRfolder/" + ricettaId +".png";
    Path path = FileSystems.getDefault().getPath(chosenPath);
    MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);
  }
}