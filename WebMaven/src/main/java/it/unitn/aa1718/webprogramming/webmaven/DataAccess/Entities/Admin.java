package it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities;
/**
 *
 * @author andreabalasso
 */
public class Admin {
  private String id;
  private String password;
  private String provincia;

  public Admin() {}

  public Admin(String id, String password, String provincia) {
    this.id = id;
    this.password = password;
    this.provincia = provincia;
  }
  
  @Override
  public String toString() {
    return "Admin[" + getId() + "," + getProvincia() +"]";
  }
  /**
   * @return the id
   */
  public String getId() {
    return id;
  }
  /**
   * @param id the id to set
   */
  public void setId(String id) {
    this.id = id;
  }
  /**
   * @return the password
   */
  public String getPassword() {
    return password;
  }
  /**
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = password;
  }
  /**
   * @return the provincia
   */
  public String getProvincia() {
    return provincia;
  }
  /**
   * @param provincia the provincia to set
   */
  public void setProvincia(String provincia) {
    this.provincia = provincia;
  }
}