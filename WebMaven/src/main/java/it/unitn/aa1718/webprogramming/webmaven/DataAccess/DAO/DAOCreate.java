package it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO;
/**
 *
 * @author andreabalasso
 */
public class DAOCreate {
    public static PazienteDAO getUtenteDAO() {
        return new PazienteDAO();
    }
    public static MedicoBDAO getMedicoBDAO() {
        return new MedicoBDAO();
    }
    public static MedicoSDAO getMedicoSDAO() {
        return new MedicoSDAO();
    }
    public static AdminDAO getAdminDAO() {
        return new AdminDAO();
    }
}