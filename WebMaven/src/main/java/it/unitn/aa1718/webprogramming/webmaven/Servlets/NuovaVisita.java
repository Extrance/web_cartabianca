package it.unitn.aa1718.webprogramming.webmaven.Servlets;

import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database;
import it.unitn.aa1718.webprogramming.webmaven.SendMail;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.mail.EmailException;
/**
 *
 * @author andreabalasso
 */
public class NuovaVisita extends HttpServlet{
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
      HttpSession session = request.getSession();
      if(session.getAttribute("username")==null || !session.getAttribute("ruolo").equals("Medico di Base"))
        request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
      else
        request.getRequestDispatcher("nuovavisita.jsp").forward(request, response);
    }
  }
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();
    if(session.getAttribute("username")==null || !session.getAttribute("ruolo").equals("Medico di Base"))
      request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
    else {
      try {
        String medico = (String) session.getAttribute("username");
        String paziente = (String) request.getParameter("paziente");
        String note = (String) request.getParameter("anamnesi");
        note = note.replace("'", "`");
        note = note.replace("\"", "``");
        
        String email = "";
       
        java.util.Date utilDate = new java.util.Date();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        Date data = sqlDate;
        
        Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
        Connection conn = db.getConnection();
        PreparedStatement st1;
        st1 = conn.prepareStatement("SELECT email FROM Pazienti WHERE (Pazienti.codice_fiscale= ? OR Pazienti.email = ?) AND id_medico= ?");
        st1.setString(1, paziente);
        st1.setString(2, paziente);
        st1.setString(3, medico);
        
        ResultSet rs1 = st1.executeQuery();
        if(rs1.next()) {
          email = rs1.getString(1);    
          if(rs1.next()) {  //Impossibile, ma non si sa mai
            session.setAttribute("errorMessage","Entry duplicata per la stessa email");
            throw new SQLException("Entry duplicata per la stessa email");
          }
        } else {
          session.setAttribute("errorMessage","Nessuna corrispondenza trovata");
          throw new SQLException("Nessuna corrispondenza trovata");
        }
        //INSERT
        PreparedStatement st;
        st = conn.prepareStatement("INSERT INTO Visita (id_medico,id_paziente,data,note) VALUES (?,?,?,?)");
        st.setString(1, medico);
        st.setString(2, email);
        st.setDate(3, data);
        st.setString(4, note);
        int rs = st.executeUpdate();
        //CHECK ID
        PreparedStatement st2;
        st2 = conn.prepareStatement("SELECT id FROM Visita WHERE id_medico=? AND id_paziente=? ORDER BY id DESC");
        st2.setString(1, medico);
        st2.setString(2, email);
        ResultSet rs2 = st2.executeQuery();
        int codice = -1;
        if(rs2.next())
          codice = rs2.getInt(1);
        
        SendMail.nuovaVisitaMail(email);

        session.setAttribute("esitoInserimento","Visita creata correttamente");
        response.sendRedirect("MedicoBasePage");
      }catch (IOException | SQLException | EmailException e) {
        response.sendRedirect("NuovaVisita");
      }
    }
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Servlet per la gestione della creazione di una visita da parte di un medico di base";
  }// </editor-fold>
}