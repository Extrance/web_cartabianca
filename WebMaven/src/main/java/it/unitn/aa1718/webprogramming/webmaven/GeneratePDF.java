package it.unitn.aa1718.webprogramming.webmaven;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.text.DocumentException;
import com.itextpdf.layout.element.Paragraph;  
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.itextpdf.layout.Document; 
import com.itextpdf.layout.element.Cell; 
import com.itextpdf.layout.element.Table; 
import com.itextpdf.kernel.pdf.PdfDocument; 
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.element.Image;  
import org.apache.commons.mail.EmailException;
/**
 *
 * @author andreabalasso
 */
public class GeneratePDF {    //migliorare impaginazione e dati della ricetta
  
  GeneratePDF() {}

  public static void generatePDFFun(int ricettaId, String path) throws FileNotFoundException, DocumentException, IOException, SQLException {
    //Generate PDF document named after the given ID
    String dest = path + "ricette/" + ricettaId + ".pdf";
    PdfWriter writer = new PdfWriter(dest);            
    PdfDocument pdf = new PdfDocument(writer);   
    // Creating a Document object       
    Document doc = new Document(pdf);
    
    String chunk = "Ricetta numero "+ricettaId+"\n";
    doc.add(new Paragraph(chunk));
    
    //select from DB
    Database db3 = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
    Connection conn3 = db3.getConnection();        
    PreparedStatement st3;
    st3 = conn3.prepareStatement("SELECT F.nome, R2.quantita  FROM Ricette2 AS R2, Farmaci AS F WHERE R2.id_ric = ? AND F.id = R2.id_farmaco ORDER BY R2.id_ric");
    st3.setInt(1, ricettaId);
    ResultSet rs3 = st3.executeQuery();
    String nome = "";
    int quantita = -1;

    while(rs3.next()) {
      nome = rs3.getString(1);
      quantita = rs3.getInt(2);
      String chunk2 = "Farmaco: "+nome+"\nQuantita': "+quantita+"\n";
      doc.add(new Paragraph(chunk2));
    }
    
    //Define what QR to select from the QRfolder. It will SURELY exist since we'll create it BEFORE creating the PDF
    String fileNameAbs = path + "QRfolder/" + ricettaId + ".png";
    ImageData data = ImageDataFactory.create(fileNameAbs);
    Image img = new Image(data); 
    img.scaleToFit(200, 200);

    doc.add(img);
    doc.close();
  }
  
  public static void generateTicketRep(String user, String path, String cf) throws FileNotFoundException, DocumentException, IOException, SQLException, EmailException {
    
    Database db3 = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
    Connection conn3 = db3.getConnection();        
    PreparedStatement st3;
    st3 = conn3.prepareStatement("SELECT P.id, E.nome, P.id_medicoS, P.data FROM Prescrizioni AS P, Esami AS E WHERE P.ticket=true AND  E.id=P.id_esame AND P.erogato=1 AND P.id_paziente= ?");
    st3.setString(1, user);
    ResultSet rs3 = st3.executeQuery();
    
    PreparedStatement st4;
    st4 = conn3.prepareStatement("SELECT P.id, E.nome, P.id_medicoS, P.data FROM Prescrizioni AS P, Esami AS E WHERE P.ticket=false AND E.id=P.id_esame AND P.erogato=true AND P.id_paziente= ?");
    st4.setString(1, user);
    ResultSet rs4 = st4.executeQuery();
    
    //Generate PDF document named after the given ID
    String dest = path + "ticket/" + cf + ".pdf";
    PdfWriter writer = new PdfWriter(dest);            
    PdfDocument pdf = new PdfDocument(writer);                  
      
    // Creating a Document object       
    Document doc = new Document(pdf);   
         
    // Creating a table       
    float [] pointColumnWidths = {80F, 150F, 120F, 100F};   
    Table table = new Table(pointColumnWidths);    

    // Adding cells to the table       
    table.addCell(new Cell().add("ID"));       
    table.addCell(new Cell().add("Esame"));       
    table.addCell(new Cell().add("Medico Specialista"));       
    table.addCell(new Cell().add("Data"));
    
    int pagateCounter = 0;
    
    while(rs3.next()) {
      table.addCell(new Cell().add(""+rs3.getInt(1)+""));
      table.addCell(new Cell().add(""+rs3.getString(2)+""));
      table.addCell(new Cell().add(""+rs3.getString(3)+""));
      table.addCell(new Cell().add(""+rs3.getDate(4)+""));
      pagateCounter++;
    }
    
    Table table2 = new Table(pointColumnWidths);
    
    table2.addCell(new Cell().add("ID"));       
    table2.addCell(new Cell().add("Esame"));       
    table2.addCell(new Cell().add("Medico Specialista"));       
    table2.addCell(new Cell().add("Data"));
    
    int nonPagateCounter = 0;
    
    while(rs4.next()) {
      table2.addCell(new Cell().add(""+rs4.getInt(1)+""));
      table2.addCell(new Cell().add(""+rs4.getString(2)+""));
      table2.addCell(new Cell().add(""+rs4.getString(3)+""));
      table2.addCell(new Cell().add(""+rs4.getDate(4)+""));
      nonPagateCounter++;
    }
    
    String titleS = "Ticket del paziente: "+cf+"\n\n";
    String pagatiS = "Ticket pagati\n";
    String nonPagatiS = "Ticket da pagare\n";
    String lineS = "\n\n";
    String totPagS = "Totale pagato: "+(pagateCounter*50)+"€\n";
    String totNonPagS = "Cifra ancora da saldare: "+(nonPagateCounter*50)+"€\n";
    Paragraph par = new Paragraph(titleS);
    Paragraph pagati = new Paragraph(pagatiS);
    Paragraph nonPagati = new Paragraph(nonPagatiS);
    Paragraph line = new Paragraph(lineS);
    Paragraph totPag = new Paragraph(totPagS);
    Paragraph totNonPag = new Paragraph(totNonPagS);
      
    doc.add(par);
    doc.add(pagati);
    doc.add(table);
    doc.add(line);
    doc.add(nonPagati);
    doc.add(table2);
    doc.add(line);
    doc.add(totPag);
    doc.add(totNonPag);
    doc.close();
    
    SendMail.scaricaTicket(user, dest);
  }
}