package it.unitn.aa1718.webprogramming.webmaven.Servlets;

import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 *
 * @author andreabalasso
 */
public class RefertoPaziente extends HttpServlet{
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      response.setContentType("text/html;charset=UTF-8");
      HttpSession session = request.getSession();
      if(session.getAttribute("username")==null || (!session.getAttribute("ruolo").equals("Medico di Base") && !session.getAttribute("ruolo").equals("Medico Specialista")))
        request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);   
      else {
        if(session.getAttribute("ruolo").equals("Medico di Base")) {
          if(request.getParameter("paziente")==null) {
            session.setAttribute("errorMessage", "nessun paziente selezionato");
            request.getRequestDispatcher("listapazientiMB.jsp").forward(request, response);
          }
          else
            request.getRequestDispatcher("refertoMB.jsp").forward(request, response);
        }
        else if(session.getAttribute("ruolo").equals("Medico Specialista")) {
          if(request.getParameter("paziente")==null) {
            session.setAttribute("errorMessage", "nessun paziente selezionato");
            request.getRequestDispatcher("listapazientiMS.jsp").forward(request, response);
          }
          else
            request.getRequestDispatcher("refertoMS.jsp").forward(request, response);
        }
      }
    } catch (IOException | ServletException ex) {
      response.sendRedirect("/");
    }
  }
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws java.io.IOException
   * @throws javax.servlet.ServletException
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    HttpSession session = request.getSession();
    
    try {
      if(session.getAttribute("ruolo")==null || (!session.getAttribute("ruolo").equals("Medico di Base") && !session.getAttribute("ruolo").equals("Medico Specialista")))
        request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
      else {
        if(request.getParameter("paziente")==null)
          response.sendRedirect("ListaPazienti");
        
        Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
        Connection conn = db.getConnection();
        PreparedStatement st9;
        
        if(session.getAttribute("ruolo").equals("Medico di Base")) {
          st9 = conn.prepareStatement("SELECT nome, cognome, email, codice_fiscale, luogo_nascita, data_nascita FROM Pazienti WHERE email= ? OR codice_fiscale= ? AND id_medico=?");
          st9.setString(1, request.getParameter("paziente"));
          st9.setString(2, request.getParameter("paziente"));
          st9.setString(3, (String) session.getAttribute("username"));
          ResultSet rs9 = st9.executeQuery();
          if(!rs9.next()) {
            request.getSession().setAttribute("paziente", request.getParameter("paziente"));
            response.sendRedirect("ListaPazienti");
          } else
            request.getRequestDispatcher("refertoMB.jsp").forward(request, response);
        } else if(session.getAttribute("ruolo").equals("Medico Specialista")) {
          st9 = conn.prepareStatement("SELECT nome, cognome, email, codice_fiscale, luogo_nascita, data_nascita FROM Pazienti WHERE email= ? OR codice_fiscale= ?");

          st9.setString(1, request.getParameter("paziente"));
          st9.setString(2, request.getParameter("paziente"));
          ResultSet rs9 = st9.executeQuery();
          if(!rs9.next()) {
            request.getSession().setAttribute("paziente", request.getParameter("paziente"));
            response.sendRedirect("ListaPazienti");
          } else
            request.getRequestDispatcher("refertoMS.jsp").forward(request, response);
        }
      }
    } catch (SQLException ex) {
      response.sendRedirect("ListaPazienti");
    }
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Servlet per la gestione del display di un referto paziente ai medici";
  }// </editor-fold>
}