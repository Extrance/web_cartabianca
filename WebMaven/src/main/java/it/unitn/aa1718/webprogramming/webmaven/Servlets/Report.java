package it.unitn.aa1718.webprogramming.webmaven.Servlets;

import it.unitn.aa1718.webprogramming.webmaven.GenerateXLS;
import java.io.IOException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 *
 * @author andreabalasso
 */
public class Report extends HttpServlet {
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();
    if(session.getAttribute("ruolo")==null || !session.getAttribute("ruolo").equals("Admin"))
      request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
    else
      request.getRequestDispatcher("report.jsp").forward(request, response);
  }
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();
    if(session.getAttribute("ruolo")==null || !session.getAttribute("ruolo").equals("Admin")){
      session.setAttribute("errorMessage","errore");
      response.sendRedirect("Report");
    }
    try {
      String fileName = request.getParameter("btnDownload");
      if(fileName==null || fileName.equals("")) {
        response.sendRedirect("Report");
      } else {

        int y = Calendar.getInstance().get(Calendar.YEAR);
        String year = ""+y;  
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int month = localDate.getMonthValue();
        String monthS = "";
        if(month<10)
          monthS = "0";
        monthS = year+"-"+monthS+month;
        
        String query = "";
        // <editor-fold defaultstate="collapsed" desc="Switch procedure. Click on the + sign on the left to edit the code.">
        switch(fileName) {
          case "Ricette":
            query = "SELECT R.id AS Ricetta, F.id AS Farmaco, F.nome AS Nome, R2.quantita AS Quantita, (case R.erogata when true then 'erogato' else 'non erogato' end) AS Erogato, R.data AS Data FROM RicTot AS R, Ricette2 AS R2, Farmaci AS F WHERE F.id=R2.id_farmaco AND R.id=R2.id_ric ORDER BY R.data, R.id";
            break;
          case "RicFarAll":
            query = "SELECT F.id AS ID, F.nome AS Nome, sum(R.quantita) AS Tot FROM Ricette2 AS R, Farmaci AS F WHERE F.id=R.id_farmaco GROUP BY F.id";
            break;
          case "RicFarYea":
            query = "SELECT F.id AS ID, F.nome AS Nome, sum(R.quantita) AS Tot FROM RicTot AS V, Ricette2 AS R, Farmaci AS F WHERE F.id=R.id_farmaco AND R.id_ric=V.id AND V.data LIKE '"+year+"%' GROUP BY F.id";
            break;
          case "RicFarMon":
            query = "SELECT F.id AS ID, F.nome AS Nome, sum(R.quantita) AS Tot FROM RicTot AS V, Ricette2 AS R, Farmaci AS F WHERE F.id=R.id_farmaco AND R.id_ric=V.id AND V.data LIKE '"+monthS+"%' GROUP BY F.id";
            break;
          case "RicPazAll":
            query = "SELECT P.codice_fiscale AS Paziente, R.id AS Ricetta, F.id AS Farmaco, F.nome AS Nome, R2.quantita AS Quantita, R.data FROM RicTot AS R, Farmaci AS F, Pazienti AS P, Ricette2 AS R2 WHERE R.id_paziente = P.email AND F.id = R2.id_farmaco AND R2.id_ric=R.id  ORDER BY P.codice_fiscale, R.id, R.data";
            break;

          case "RicPazYea":
            query = "SELECT P.codice_fiscale AS Paziente, R.id AS Ricetta, F.id AS Farmaco, F.nome AS Nome, R2.quantita AS Quantita, R.data FROM RicTot AS R, Farmaci AS F, Pazienti AS P, Ricette2 AS R2 WHERE R.id_paziente = P.email AND F.id = R2.id_farmaco AND R2.id_ric=R.id AND R.data LIKE '"+year+"%' ORDER BY P.codice_fiscale, R.id, R.data";
            break;
          case "RicPazMon":
            query = "SELECT P.codice_fiscale AS Paziente, R.id AS Ricetta, F.id AS Farmaco, F.nome AS Nome, R2.quantita AS Quantita, R.data FROM RicTot AS R, Farmaci AS F, Pazienti AS P, Ricette2 AS R2 WHERE R.id_paziente = P.email AND F.id = R2.id_farmaco AND R2.id_ric=R.id AND R.data LIKE '"+monthS+"%'  ORDER BY P.codice_fiscale, R.id, R.data";
            break;
          case "RicDatAll":
            query = "SELECT R.data AS 'Data Ricetta', R.id AS Ricetta, F.id AS Farmaco, F.nome AS Nome, R2.quantita AS Quantita FROM RicTot AS R, Ricette2 AS R2, Farmaci AS F WHERE R2.id_farmaco=F.id AND R.id=R2.id_ric ORDER BY R.data, R.id";
            break;
          case "RicDatYea":
            query = "SELECT R.data AS 'Data Ricetta', R.id AS Ricetta, F.id AS Farmaco, F.nome AS Nome, R2.quantita AS Quantita FROM RicTot AS R, Ricette2 AS R2, Farmaci AS F WHERE R2.id_farmaco=F.id AND R.id=R2.id_ric AND R.data LIKE '"+year+"%' ORDER BY R.data, R.id";
            break;
          case "RicDatMon":
            query = "SELECT R.data AS 'Data Ricetta', R.id AS Ricetta, F.id AS Farmaco, F.nome AS Nome, R2.quantita AS Quantita FROM RicTot AS R, Ricette2 AS R2, Farmaci AS F WHERE R2.id_farmaco=F.id AND R.id=R2.id_ric AND R.data LIKE '"+monthS+"%' ORDER BY R.data, R.id";
            break;
          case "RicMBAll":
            query = "SELECT M.matricola AS Medico, R.id AS Ricetta, F.id AS Farmaco, F.nome AS Nome, R2.quantita AS Quantita, R.data AS Data FROM Farmaci AS F, RicTot AS R, Ricette2 AS R2, MediciB AS M WHERE R.id=R2.id_ric AND R2.id_farmaco=F.id AND M.email=R.id_medico ORDER BY M.matricola, R.id, F.id";
            break;
          case "RicMBYea":
            query = "SELECT M.matricola AS Medico, R.id AS Ricetta, F.id AS Farmaco, F.nome AS Nome, R2.quantita AS Quantita, R.data AS Data FROM Farmaci AS F, RicTot AS R, Ricette2 AS R2, MediciB AS M WHERE R.id=R2.id_ric AND R2.id_farmaco=F.id AND M.email=R.id_medico AND R.data LIKE '"+year+"%' ORDER BY M.matricola, R.id, F.id";
            break;
          case "RicMBMon":
            query = "SELECT M.matricola AS Medico, R.id AS Ricetta, F.id AS Farmaco, F.nome AS Nome, R2.quantita AS Quantita, R.data AS Data FROM Farmaci AS F, RicTot AS R, Ricette2 AS R2, MediciB AS M WHERE R.id=R2.id_ric AND R2.id_farmaco=F.id AND M.email=R.id_medico AND R.data LIKE '"+monthS+"%' ORDER BY M.matricola, R.id, F.id";
            break;
          case "Esami":
            query = "SELECT E.id AS ID, E.nome AS Esame, M.matricola AS 'Medico di Base', (case Pr.erogato when true then 'erogato' else 'non erogato' end) AS Erogato, (case Pr.ticket when true then 'pagato' else 'non pagato' end) AS Ticket, Pr.dataV AS 'Data Prescrizione', IFNULL(Pr.data,'non erogato') AS 'Data Esame' FROM Prescrizioni AS Pr, Esami AS E, MediciB AS M WHERE Pr.id_esame=E.id AND M.email=Pr.id_medicoB ORDER BY Pr.dataV";
            break;
          case "PreExaAll":
            query = "SELECT E.id AS ID, E.nome AS Nome, count(*) AS Prescritti, count(case Pr.erogato when true then 1 else null end) AS Erogati, (count(case Pr.erogato when true then 1 else null end)*50) AS Introito FROM Prescrizioni AS Pr, Esami AS E WHERE Pr.id_esame=E.id GROUP BY E.id";
            break;
          case "PreExaYea":
            query = "SELECT E.id AS ID, E.nome AS Nome, count(*) AS Prescritti, count(case Pr.erogato when true then 1 else null end) AS Erogati, (count(case Pr.erogato when true then 1 else null end)*50) AS Introito FROM Prescrizioni AS Pr, Esami AS E WHERE Pr.id_esame=E.id AND Pr.dataV LIKE '"+year+"%' GROUP BY E.id";
            break;
          case "PreExaMon":
            query = "SELECT E.id AS ID, E.nome AS Nome, count(*) AS Prescritti, count(case Pr.erogato when true then 1 else null end) AS Erogati, (count(case Pr.erogato when true then 1 else null end)*50) AS Introito FROM Prescrizioni AS Pr, Esami AS E WHERE Pr.id_esame=E.id AND Pr.dataV LIKE '"+monthS+"%' GROUP BY E.id";
            break;
          case "PrePazAll":
            query = "SELECT P.codice_fiscale AS Paziente, E.id AS ID, E.nome AS Esame, (case Pr.erogato when true then 'erogato' else 'non erogato' end) AS Erogato, (case (Pr.erogato) when true then (case (Pr.ticket) when true then '50€' else '0€' end) else 'non erogato' end) AS Incasso FROM Prescrizioni AS Pr, Pazienti AS P, Esami AS E WHERE Pr.id_paziente=P.email AND E.id=Pr.id_esame ORDER BY P.codice_fiscale";
            break;
          case "PrePazYea":
            query = "SELECT P.codice_fiscale AS Paziente, E.id AS ID, E.nome AS Esame, (case Pr.erogato when true then 'erogato' else 'non erogato' end) AS Erogato, (case (Pr.erogato) when true then (case (Pr.ticket) when true then '50€' else '0€' end) else 'non erogato' end) AS Incasso FROM Prescrizioni AS Pr, Pazienti AS P, Esami AS E WHERE Pr.id_paziente=P.email AND E.id=Pr.id_esame AND Pr.dataV LIKE '"+year+"%' ORDER BY P.codice_fiscale";
            break;
          case "PrePazMon":
            query = "SELECT P.codice_fiscale AS Paziente, E.id AS ID, E.nome AS Esame, (case Pr.erogato when true then 'erogato' else 'non erogato' end) AS Erogato, (case (Pr.erogato) when true then (case (Pr.ticket) when true then '50€' else '0€' end) else 'non erogato' end) AS Incasso FROM Prescrizioni AS Pr, Pazienti AS P, Esami AS E WHERE Pr.id_paziente=P.email AND E.id=Pr.id_esame AND Pr.dataV LIKE '"+monthS+"%' ORDER BY P.codice_fiscale";
            break;
          case "PreDatAll":
            query = "SELECT Pr.dataV, E.id, E.nome, IFNULL(Pr.data, 'non erogato') AS 'Data Erogazione' FROM Prescrizioni AS Pr, Esami AS E WHERE Pr.id_esame=E.id ORDER BY Pr.dataV, E.id";
            break;
          case "PreDatYea":
            query = "SELECT Pr.dataV, E.id, E.nome, IFNULL(Pr.data, 'non erogato') AS 'Data Erogazione' FROM Prescrizioni AS Pr, Esami AS E WHERE Pr.id_esame=E.id AND Pr.dataV LIKE '"+year+"%' ORDER BY Pr.dataV, E.id";
            break;
          case "PreDatMon":
            query = "SELECT Pr.dataV, E.id, E.nome, IFNULL(Pr.data, 'non erogato') AS 'Data Erogazione' FROM Prescrizioni AS Pr, Esami AS E WHERE Pr.id_esame=E.id AND Pr.dataV LIKE '"+monthS+"%' ORDER BY Pr.dataV, E.id";
            break;
          case "PreMBAll":
            query = "SELECT M.matricola AS 'Medico di Base', E.id AS ID, E.nome AS Esame, (case Pr.erogato when true then 'erogato' else 'non erogato' end) AS Erogato, Pr.dataV AS 'Data Prescrizione' FROM Prescrizioni AS Pr, MediciB AS M, Esami AS E WHERE Pr.id_medicoB=M.email AND Pr.id_esame=E.id ORDER BY M.matricola, Pr.dataV";
            break;
          case "PreMBYea":
            query = "SELECT M.matricola AS 'Medico di Base', E.id AS ID, E.nome AS Esame, (case Pr.erogato when true then 'erogato' else 'non erogato' end) AS Erogato, Pr.dataV AS 'Data Prescrizione' FROM Prescrizioni AS Pr, MediciB AS M, Esami AS E WHERE Pr.id_medicoB=M.email AND Pr.id_esame=E.id AND Pr.dataV LIKE '"+year+"%' ORDER BY M.matricola, Pr.dataV";
            break;
          case "PreMBMon":
            query = "SELECT M.matricola AS 'Medico di Base', E.id AS ID, E.nome AS Esame, (case Pr.erogato when true then 'erogato' else 'non erogato' end) AS Erogato, Pr.dataV AS 'Data Prescrizione' FROM Prescrizioni AS Pr, MediciB AS M, Esami AS E WHERE Pr.id_medicoB=M.email AND Pr.id_esame=E.id AND Pr.dataV LIKE '"+monthS+"%' ORDER BY M.matricola, Pr.dataV";
            break;
          case "ExaMSAll":
            query = "SELECT M.matricola, E.id, E.nome, Pr.data, (case Pr.ticket when true then '50€' else 'non riscosso' end) AS Ticket FROM Prescrizioni AS Pr, Esami AS E, MediciS AS M WHERE Pr.id_esame=E.id AND Pr.erogato=true AND Pr.id_medicoS=M.email ORDER BY M.matricola, Pr.data";
            break;
          case "ExaMSYea":
            query = "SELECT M.matricola, E.id, E.nome, Pr.data, (case Pr.ticket when true then '50€' else 'non riscosso' end) AS Ticket FROM Prescrizioni AS Pr, Esami AS E, MediciS AS M WHERE Pr.id_esame=E.id AND Pr.erogato=true AND Pr.id_medicoS=M.email AND Pr.data LIKE '"+year+"%' ORDER BY M.matricola, Pr.data";
            break;
          case "ExaMSMon":
            query = "SELECT M.matricola, E.id, E.nome, Pr.data, (case Pr.ticket when true then '50€' else 'non riscosso' end) AS Ticket FROM Prescrizioni AS Pr, Esami AS E, MediciS AS M WHERE Pr.id_esame=E.id AND Pr.erogato=true AND Pr.id_medicoS=M.email AND Pr.data LIKE '"+monthS+"%' ORDER BY M.matricola, Pr.data";
            break;
          //FIN QUI OK
          case "ExaDatAll":
            query = "SELECT Pr.data AS 'Data erogazione', E.id, E.nome, P.codice_fiscale AS Paziente, M.matricola AS 'Medico Specialista', (case Pr.ticket when true then '50€' else 'non riscosso' end) AS Ticket FROM Prescrizioni AS Pr, Esami AS E, Pazienti AS P, MediciS AS M WHERE Pr.id_esame=E.id AND Pr.erogato=true AND Pr.id_paziente=P.email AND Pr.id_medicoS=M.email ORDER BY Pr.data";
            break;
          case "ExaDatYea":
            query = "SELECT Pr.data AS 'Data erogazione', E.id, E.nome, P.codice_fiscale AS Paziente, M.matricola AS 'Medico Specialista', (case Pr.ticket when true then '50€' else 'non riscosso' end) AS Ticket FROM Prescrizioni AS Pr, Esami AS E, Pazienti AS P, MediciS AS M WHERE Pr.id_esame=E.id AND Pr.erogato=true AND Pr.id_paziente=P.email AND Pr.id_medicoS=M.email AND Pr.data LIKE '"+year+"%' ORDER BY Pr.data";
            break;
          case "ExaDatMon":
            query = "SELECT Pr.data AS 'Data erogazione', E.id, E.nome, P.codice_fiscale AS Paziente, M.matricola AS 'Medico Specialista', (case Pr.ticket when true then '50€' else 'non riscosso' end) AS Ticket FROM Prescrizioni AS Pr, Esami AS E, Pazienti AS P, MediciS AS M WHERE Pr.id_esame=E.id AND Pr.erogato=true AND Pr.id_paziente=P.email AND Pr.id_medicoS=M.email AND Pr.data LIKE '"+monthS+"%' ORDER BY Pr.data";
            break;
          default:
            response.sendRedirect("Report");
            break;  //NOT NEEDED
        }// </editor-fold>

        String pathAppoggio = this.getClass().getClassLoader().getResource("").getPath();
        String fullPath = URLDecoder.decode(pathAppoggio, "UTF-8");
        String pathArr[] = fullPath.split("target/");
        fullPath = pathArr[0];
        fullPath = fullPath + "src/main/privateImg/report/";

        int success = GenerateXLS.generateDoc(fullPath, fileName, query);
        if(success==0) {
          request.setAttribute("fileName", fileName);
          request.getRequestDispatcher("DownloadServlet").forward(request, response);
        }
        else
          session.setAttribute("errorMessage","errore");
        response.sendRedirect("Report");
      }
    } catch(IOException | SQLException | ClassNotFoundException e) {
      session.setAttribute("errorMessage","errore");
    }
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>
}