package it.unitn.aa1718.webprogramming.webmaven.Servlets.Login;

import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.AdminDAO;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.DAOCreate;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Admin;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.MedicoB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 *
 * @author andreabalasso
 */
public class ServizioSanitario extends HttpServlet {
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    request.getRequestDispatcher("privateLogin.jsp").forward(request, response);
  }
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();
    if (session.getAttribute("username") != null) {
      response.setStatus(500);    //Errore con utenti già loggati che accedono in post alla servlet
    }
    String idOperatore = request.getParameter("idOperatore");
    String password = request.getParameter("password");
    
    AdminDAO utd = DAOCreate.getAdminDAO();
    
    try {
      //controllo preliminare esistenza della mail nel db
      if(utd.isUsed(idOperatore)) {
        Admin currentuser = utd.getUtenteByEmail(idOperatore);

        if (password.equals(currentuser.getPassword())) {
          //dati inseriti combaciano, creo la sessione
          session.setAttribute("username", idOperatore);
          session.setAttribute("ruolo", "Admin");
          session.setAttribute("provincia", currentuser.getProvincia());
          session.removeAttribute("errorMessage");
          
          response.sendRedirect("AdminPage");
        } else {
          session.setAttribute("errorMessage", "La combinazione email/password è sbagliata");
          response.sendRedirect("ReservedLogin");
        }
      } else {
        session.setAttribute("errorMessage", "L'username non e' corretto");
        response.sendRedirect("ReservedLogin");
      }
    } catch (SQLException ex) {
      session.setAttribute("errorMessage", "Errore di accesso al DB");
      response.sendRedirect("ReservedLogin");
    }
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>
}