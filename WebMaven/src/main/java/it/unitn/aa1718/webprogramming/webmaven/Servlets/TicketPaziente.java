package it.unitn.aa1718.webprogramming.webmaven.Servlets;

import com.itextpdf.text.DocumentException;
import it.unitn.aa1718.webprogramming.webmaven.GeneratePDF;
import it.unitn.aa1718.webprogramming.webmaven.SendMail;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.mail.EmailException;
/**
 *
 * @author andreabalasso
 */
public class TicketPaziente extends HttpServlet {
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
      HttpSession session = request.getSession();
      if(session.getAttribute("ruolo")!=null && session.getAttribute("ruolo").equals("Paziente"))
        request.getRequestDispatcher("ticketpaziente.jsp").forward(request, response);
      else
        request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
    }
  }
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   * @throws java.io.FileNotFoundException
   * @throws java.net.MalformedURLException
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, FileNotFoundException, MalformedURLException {
    HttpSession session = request.getSession();
    if(session.getAttribute("ruolo")!=null && session.getAttribute("ruolo").equals("Paziente")) {
      String pathAppoggio = this.getClass().getClassLoader().getResource("").getPath();
      String fullPath = URLDecoder.decode(pathAppoggio, "UTF-8");
      String pathArr[] = fullPath.split("target/");
      fullPath = pathArr[0];
      fullPath = fullPath + "src/main/privateImg/";
      try {
        GeneratePDF.generateTicketRep((String) session.getAttribute("username"), fullPath, (String) session.getAttribute("cf"));
        session.setAttribute("stampaTicket", "ok");
      } catch (DocumentException | IOException | SQLException | EmailException ex) {
        request.getRequestDispatcher("ticketpaziente.jsp").forward(request, response);
      }
      request.getRequestDispatcher("ticketpaziente.jsp").forward(request, response);
    } 
    else
      request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>
}