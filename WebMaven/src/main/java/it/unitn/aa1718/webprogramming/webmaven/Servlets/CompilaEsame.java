package it.unitn.aa1718.webprogramming.webmaven.Servlets;

import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database;
import it.unitn.aa1718.webprogramming.webmaven.SendMail;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.mail.EmailException;
/**
 *
 * @author andreabalasso
 */
public class CompilaEsame extends HttpServlet {
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
      HttpSession session = request.getSession();
      if(session.getAttribute("ruolo")!=null && session.getAttribute("ruolo").equals("Medico Specialista"))
        request.getRequestDispatcher("compilaMS.jsp").forward(request, response);
      else
        request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
    }
  }
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();
    try {
      String medico = (String) session.getAttribute("username");
      String paziente = request.getParameter("paziente");
      String prescrizioneS = request.getParameter("prescrizione");
      String referto = (String) request.getParameter("referto");
      
      referto = referto.replace("'", "`");
      referto = referto.replace("\"", "``");
      
      String[] checked = request.getParameterValues("ticket");
      String email= "";
      
      java.util.Date utilDate = new java.util.Date();
      java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
      Date data = sqlDate;
      
      Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
      Connection conn = db.getConnection(); 
      PreparedStatement st1 = conn.prepareStatement("SELECT DISTINCT P.email FROM Prescrizioni AS Pr, Pazienti AS P WHERE Pr.id_paziente=P.email AND (P.email=? OR P.codice_fiscale=?)");
      st1.setString(1, paziente);
      st1.setString(2, paziente);
      ResultSet rs1 = st1.executeQuery();
      if(rs1.next()) {
        email = rs1.getString(1);
        if(rs1.next()) {  //Impossibile, ma non si sa mai
          session.setAttribute("errorMessage","Entry duplicata per la stessa email");
          throw new SQLException("Entry duplicata per la stessa email");
        }
      } else {
        session.setAttribute("errorMessage","Nessuna corrispondenza trovata");
        throw new SQLException("Nessuna corrispondenza trovata");
      }
      
      PreparedStatement st2 = conn.prepareStatement("SELECT Pr.id, E.nome FROM Prescrizioni AS Pr, Esami AS E, Pazienti AS P WHERE Pr.id_esame=E.id AND Pr.id_paziente=P.email AND P.email=?");
      st2.setString(1, email);
      ResultSet rs2 = st2.executeQuery();
      int prescrizione = -1;
      while(rs2.next()) {
        String tmp = ""+rs2.getInt(1)+"-"+rs2.getString(2);
        if(tmp.equals(prescrizioneS))
          prescrizione = rs2.getInt(1);
      }
      
      PreparedStatement st;
      st = conn.prepareStatement("UPDATE Prescrizioni SET referto = ?, id_medicoS= ?, erogato= ?, ticket= ?, data= ? WHERE id= ?");   //OK
      st.setString(1, referto);
      st.setString(2, medico);
       st.setBoolean(3, true);
      if(checked==null)
        st.setInt(4, 0);
      else
        st.setInt(4, 1);
      st.setDate(5, data);
      st.setInt(6, prescrizione);
      int rs = st.executeUpdate();

      if(rs==0) {
        session.setAttribute("errorMessage","Errore nello scambio dati");
        throw new SQLException("Nessuna corrispondenza trovata");
      }
      else {
        SendMail.nuovoEsitoMail(email);
        session.setAttribute("esitoInserimento","Esame compilato correttamente");
      }
      response.sendRedirect("MedicoSpecPage");
    } catch(IOException | NumberFormatException | SQLException | EmailException e) {
      if(session.getAttribute("errorMessage")==null)
        session.setAttribute("errorMessage","Errore nello scambio dati");
      response.sendRedirect("CompilaEsame");
    }
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Servlet per la compilazione di un esame da parte di un medico specialista";
  }// </editor-fold>
}