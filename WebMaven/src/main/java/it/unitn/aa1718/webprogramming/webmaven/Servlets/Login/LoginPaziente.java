package it.unitn.aa1718.webprogramming.webmaven.Servlets.Login;

import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.DAOCreate;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.PazienteDAO;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Paziente;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
/**
 *
 * @author andreabalasso
 */
public class LoginPaziente extends HttpServlet {
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
      HttpSession session = request.getSession();
      if(session.getAttribute("ruolo")==null || !session.getAttribute("ruolo").equals("Paziente")) {
        request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);   
      } else {
        response.sendRedirect("PazientePage");   
      }
    }
  }
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();
    if (session.getAttribute("username") != null) {
      response.setStatus(500);    //Errore con utenti già loggati che accedono in post alla servlet
    }
    String email = request.getParameter("username");
    String password = request.getParameter("password");

    PazienteDAO utd = DAOCreate.getUtenteDAO();
        
    try {
      if (utd.isUsed(email)) {
        Paziente currentuser = utd.getUtenteByEmail(email);

        if (password.equals(currentuser.getPassword())) {
          session.setAttribute("username", email);
          session.setAttribute("ruolo", "Paziente");
          session.setAttribute("nome", currentuser.getNome());
          session.setAttribute("cognome", currentuser.getCognome());
          session.setAttribute("sesso", currentuser.getSesso());
          session.setAttribute("data_nascita", currentuser.getDataNascita());
          session.setAttribute("luogo_nascita", currentuser.getLuogoNascita());
          session.setAttribute("cf", currentuser.getCf());
          session.setAttribute("foto", currentuser.getFoto());
          session.setAttribute("id_medico", currentuser.getMedico());
          session.setAttribute("provincia", currentuser.getProvincia());
          session.removeAttribute("errorMessage");
                    
          String remember[]= request.getParameterValues("rememberme");
          //set cookies
          if(remember!=null) {
            Cookie cookiePaziente = new Cookie("usernamePaziente" , email);
            Cookie cookiePwPaziente = new Cookie("passwordPaziente" , password);
                        
            cookiePaziente.setMaxAge(31536000);     //1 year is enough
            cookiePwPaziente.setMaxAge(31536000);   //1 year is enough
                        
            response.addCookie(cookiePaziente);
            response.addCookie(cookiePwPaziente);
          }
          Object filterSavedRequestPage = session.getAttribute("filterSavedRequest");
          if (filterSavedRequestPage != null) {
            try {
              String fsrStr = (String) filterSavedRequestPage;
              session.removeAttribute("filterSavedRequest");
              response.sendRedirect("PazientePage");
            } catch (Exception e) {
              session.setAttribute("errorMessage", "La combinazione email/password è sbagliata");
              response.sendRedirect("/");
            }
          } else {
            response.sendRedirect("PazientePage");
          }
        } else {
          session.setAttribute("errorMessage", "La combinazione email/password è sbagliata");
          response.sendRedirect("/");
        }
      } else {
        session.setAttribute("errorMessage", "L'indirizzo mail non è corretto");
        response.sendRedirect("/");
      }
    } catch (SQLException ex) {
      session.setAttribute("errorMessage", "Errore di accesso al DB");
      response.sendRedirect("/");
    }
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Servlet per il login paziente";
  }// </editor-fold>
}