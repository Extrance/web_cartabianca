package it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities;
/**
 *
 * @author andreabalasso
 */
public class MedicoB {
  private String email;
  private String password;
  private String nome;
  private String cognome;
  private String citta;
  private String provincia;
  private String matricola;

  public MedicoB() {}

  public MedicoB(String email, String password, String nome, String cognome, String citta, String provincia, String matricola) {
    this.email = email;
    this.password = password;
    this.nome = nome;
    this.cognome = cognome;
    this.citta = citta;
    this.provincia = provincia;
    this.matricola = matricola;
  }

  @Override
  public String toString() {
    return "MedicoB[" + getEmail() + "," + getNome() + "," + getCognome() + "," + getCitta() + "," + getProvincia() +"]";
  }
  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }
  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }
  /**
   * @return the password
   */
  public String getPassword() {
    return password;
  }
  /**
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = password;
  }
  /**
   * @return the nome
   */
  public String getNome() {
    return nome;
  }
  /**
   * @param nome the nome to set
   */
  public void setNome(String nome) {
    this.nome = nome;
  }
  /**
   * @return the cognome
   */
  public String getCognome() {
    return cognome;
  }
  /**
   * @param cognome the cognome to set
   */
  public void setCognome(String cognome) {
    this.cognome = cognome;
  }
  /**
   * @return the citta
   */
  public String getCitta() {
    return citta;
  }
  /**
   * @param citta the citta to set
   */
  public void setCitta(String citta) {
    this.citta = citta;
  }
  /**
   * @return the provincia
   */
  public String getProvincia() {
    return provincia;
  }
  /**
   * @param provincia the provincia to set
   */
  public void setProvincia(String provincia) {
    this.provincia = provincia;
  }
  /**
   * @return the matricola
   */
  public String getMatricola() {
    return matricola;
  }
  /**
   * @param matricola the matricola to set
   */
  public void setMatricola(String matricola) {
    this.matricola = matricola;
  }
}