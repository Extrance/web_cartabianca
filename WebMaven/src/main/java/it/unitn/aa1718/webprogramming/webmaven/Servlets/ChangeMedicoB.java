package it.unitn.aa1718.webprogramming.webmaven.Servlets;

import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database;
import it.unitn.aa1718.webprogramming.webmaven.SendMail;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.mail.EmailException;
/**
 *
 * @author andreabalasso
 */
public class ChangeMedicoB extends HttpServlet{
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
      HttpSession session = request.getSession();
      if(session.getAttribute("ruolo")==null)
        request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
      else {
        if(session.getAttribute("ruolo").equals("Paziente"))
          request.getRequestDispatcher("changemedico.jsp").forward(request, response);
        else
          request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
      }
    } catch (Exception ex) {
      response.sendRedirect("/");
    }
  }
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws java.io.IOException
   * @throws javax.servlet.ServletException
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    String newMedicoB = request.getParameter("NewMedicoB");
    HttpSession session = request.getSession();
    
    if(!session.getAttribute("ruolo").equals("Paziente"))
      request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
    else {
      Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
      Connection conn = db.getConnection();
                
      PreparedStatement st;
      PreparedStatement st2;
      try {
        st = conn.prepareStatement("UPDATE Pazienti SET id_medico= ? WHERE email = ?");
        st.setString(1, newMedicoB);
        st.setString(2, (String) session.getAttribute("username"));
        int rs = st.executeUpdate();
        
        session.setAttribute("id_medico", newMedicoB);
        String user = (String) session.getAttribute("username");
        String id = (String) session.getAttribute("id_medico");
            
        st2 = conn.prepareStatement("SELECT nome, cognome FROM MediciB WHERE email= ?");
        st2.setString(1, id);
        ResultSet rs2 = st2.executeQuery();
        
        String nome = "";
        String cognome = "";
        
        while(rs2.next()) {
          nome=rs2.getString(1);
          cognome=rs2.getString(2);
        }
            
        SendMail.changeMedicoMail(user, nome, cognome, id);
        response.sendRedirect("PazientePage");
      } catch (IOException | SQLException | EmailException ex) {
        session.setAttribute("errorMessage", "Errore");
        response.sendRedirect("ChangeMedicoB");
      }
    }
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Servlet che permette al Paziente di cambiare il proprio medico di base (rimanendo nella stessa provincia)";
  }// </editor-fold>
}