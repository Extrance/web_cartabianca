package it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO;

import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Paziente;
import java.sql.*;
/**
 *
 * @author andreabalasso
 */
public class PazienteDAO extends User{
  Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
  Connection conn = db.getConnection();

  public boolean isUsed(String email) throws SQLException {    //Funzione comoda per capire se esiste già un record 'username'
    Boolean result;

    if (email != null) {
        PreparedStatement st = conn.prepareStatement("SELECT true FROM Pazienti WHERE email = ?");
        st.setString(1, email);

        ResultSet rs = st.executeQuery();
        if (rs.next()) {
            result = true;
        } else {
            result = false;
        }
    } else {
        result = false;
    }
    return result;
  }
    
  public Paziente getUtenteByEmail(String email) throws SQLException {
    Paziente result;

    if (email == null) {
        return null;
    }
    PreparedStatement st = conn.prepareStatement("select * from Pazienti where Pazienti.email = ?");
    st.setString(1, email);
    ResultSet rs = st.executeQuery();
    if (rs.next()) {
      result = new Paziente();
      result.setEmail(rs.getString(1));
      result.setPassword(rs.getString(2));
      result.setNome(rs.getString(3));
      result.setCognome(rs.getString(4));
      result.setSesso(rs.getString(5));
      result.setDataNascita(rs.getString(6));
      result.setLuogoNascita(rs.getString(7));
      result.setCf(rs.getString(8));
      result.setFoto(rs.getString(9));
      result.setMedico(rs.getString(10));
      result.setProvincia(rs.getString(11));

      if (rs.next()) {
        throw new SQLException("Entry duplicata per la stessa email");
      }
    } else {
      result = null;
    }
    return result;
  }
}