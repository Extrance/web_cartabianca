package it.unitn.aa1718.webprogramming.webmaven;

import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
/**
 *
 * @author andreabalasso
 */
public class GenerateXLS {
  
  GenerateXLS() {}
  
  public static int generateDoc(String path, String name, String query) throws SQLException, ClassNotFoundException {
  
    int esito = 0;
    try {
      XSSFWorkbook workbook = new XSSFWorkbook();
      XSSFSheet sheet = workbook.createSheet("Report");

      int y = Calendar.getInstance().get(Calendar.YEAR);
      String year = ""+y;
      
      Date date = new Date();
      LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
      int month = localDate.getMonthValue();
      String monthS = "";
      if(month<10)
        monthS = "0";
      monthS = year+"-"+monthS+month;
      
      Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
      Connection conn = db.getConnection();
      PreparedStatement st = conn.prepareStatement(query);

      int intCol=0;
      int intRow=0;
      
      switch(name) {
        case "PreMBAll": case "PreMBYea": case "PreMBMon": case "PreExaAll": case "PreExaYea": case "PreExaMon": case "RicDatAll": case "RicDatYea": case "RicDatMon": case "ExaMSAll": case "ExaMSYea": case "ExaMSMon":
          intCol = 5;
          break;
        case "RicFarAll": case "RicFarYea": case "RicFarMon":
          intCol = 3;
          break;
        case "PrePazAll": case "PrePazYea": case "PrePazMon": case "PreDatAll": case "PreDatYea": case "PreDatMon":
          intCol = 4;
          break;
        case "Ricette": case "RicPazAll": case "RicPazYea": case "RicPazMon": case "RicMBAll": case "RicMBYea": case "RicMBMon": case "ExaDatAll": case "ExaDatYea": case "ExaDatMon":
          intCol = 6;
          break;
        case "Esami":
          intCol = 7;
          break;
        default:
          throw new SQLException("err");
      }
      
      ResultSet rs = st.executeQuery();
      while(rs.next())
        intRow+=1;

      int rowCount = 0;

      Object b = null;
      String a="";
      ResultSet rs1 = st.executeQuery();
      ResultSetMetaData rsmd = rs1.getMetaData();
      
      //DEFINE TITLES
      Row rowTitle = sheet.createRow(++rowCount);
      int columnCountTitle = 0;
      for(int w=1; w<=intCol; w++) {
        Cell cellTitle = rowTitle.createCell(++columnCountTitle);
        cellTitle.setCellValue((String) rsmd.getColumnLabel(w));
      }
      
      for (int i=0; i<intRow; i++) {
        Row row = sheet.createRow(++rowCount);

        int columnCount = 0;
        rs1.next();
        for (int j=1; j<=intCol; j++) {
          Cell cell = row.createCell(++columnCount);
          b = rs1.getObject(j);
          a = ""+b;
          cell.setCellValue((String) a);
        }
      }
      FileOutputStream outputStream = new FileOutputStream(path+name+".xlsx");
      workbook.write(outputStream);
    } catch(SQLException | IOException e) {
      esito = -1;
    }
    return esito;
  }
}