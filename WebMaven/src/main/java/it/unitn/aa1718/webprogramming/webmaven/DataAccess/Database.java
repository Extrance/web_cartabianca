package it.unitn.aa1718.webprogramming.webmaven.DataAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 *
 * @author andreabalasso
 */
public class Database {
  private Connection myConn;
  private static final Database INSTANCE = new Database();

  private Database() {
    try {
      Class.forName("com.mysql.jdbc.Driver"); //Questa riga è molto importante per comunicare a TomCat di includere la classe Driver del database
      myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ServiziSanitari", "root", "mysqlpw"); //3306 porta standard di mysql
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("ERRORE: IMPOSSIBILE CONNETTERSI AL DATABASE");
    }
  }

  public static Database getInstance() {
    return INSTANCE;
  }
    
  public Connection getConnection() {
    return myConn;
  }
}