package it.unitn.aa1718.webprogramming.webmaven.Servlets;

import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.DAOCreate;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.PazienteDAO;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Paziente;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 *
 * @author andreabalasso
 */
//DA FARE!!!!!
public class CercaEsame extends HttpServlet{
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws java.io.IOException
   * @throws javax.servlet.ServletException
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    HttpSession session = request.getSession();
    PazienteDAO utd = DAOCreate.getUtenteDAO();
    try {
      Paziente req = utd.getUtenteByEmail((String) session.getAttribute("username"));
      if(req==null)
        throw new Exception("err");
      else {
        Database db = Database.getInstance();
        Connection conn = db.getConnection();
        PreparedStatement st = conn.prepareStatement("SELECT * FROM Esami WHERE nome LIKE '%"+request.getParameter("esame")+"%' OR descrizione LIKE '%"+request.getParameter("esame")+"%'");
        request.getRequestDispatcher("examlist.jsp").forward(request, response);
      }
    } catch (Exception ex) {
      request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
    }
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "DA FARE";
  }// </editor-fold>
}