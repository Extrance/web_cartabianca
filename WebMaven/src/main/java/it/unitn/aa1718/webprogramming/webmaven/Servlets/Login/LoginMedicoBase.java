package it.unitn.aa1718.webprogramming.webmaven.Servlets.Login;

import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.DAOCreate;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.MedicoBDAO;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.MedicoB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
/**
 *
 * @author andreabalasso
 */
public class LoginMedicoBase extends HttpServlet {
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
      HttpSession session = request.getSession();
      if(session.getAttribute("ruolo")==null || !session.getAttribute("ruolo").equals("Medico di Base"))
        request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
      else
        response.sendRedirect("MedicoBasePage");
    }
  }
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();
    //controllo se esiste gia' una sessione attiva
    if (session.getAttribute("username") != null)
      response.setStatus(500);
    
    String email = request.getParameter("username");
    String password = request.getParameter("password");

    MedicoBDAO utd = DAOCreate.getMedicoBDAO();
        
    try {
      //controllo preliminare esistenza della mail nel db
      if(utd.isUsed(email)) {
        MedicoB currentuser = utd.getUtenteByEmail(email);

        if (password.equals(currentuser.getPassword())) {
          //dati inseriti combaciano, creo la sessione
          session.setAttribute("username", email);
          session.setAttribute("ruolo", "Medico di Base");
          session.setAttribute("nome", currentuser.getNome());
          session.setAttribute("cognome", currentuser.getCognome());
          session.setAttribute("citta", currentuser.getCitta());
          session.setAttribute("provincia", currentuser.getProvincia());
          session.setAttribute("matricola", currentuser.getMatricola());
          session.removeAttribute("errorMessage");
                    
          //controllo checkbox ricordami
          String remember[]= request.getParameterValues("rememberme");
          if(remember!=null) {
            //creo i cookie
            Cookie cookieMedicoBase = new Cookie("usernameMB",email);
            Cookie cookiePwMB = new Cookie("passwordMB",password);
            //imposto la validita'
            cookieMedicoBase.setMaxAge(31536000);   //1 year is enough
            cookiePwMB.setMaxAge(31536000);         //1 year is enough
            //aggiungo i cookie alla response
            response.addCookie(cookieMedicoBase);
            response.addCookie(cookiePwMB);
          }

          Object filterSavedRequestPage = session.getAttribute("filterSavedRequest");
          if (filterSavedRequestPage != null) {
            try {
              String fsrStr = (String) filterSavedRequestPage;
              session.removeAttribute("filterSavedRequest");
              response.sendRedirect("MedicoBasePage");
            } catch (Exception e) {
              response.sendRedirect("/");
            }
          } else {
            response.sendRedirect("MedicoBasePage");
          }
        } else {
          session.setAttribute("errorMessage", "La combinazione email/password è sbagliata");
          response.sendRedirect("/");
        }
      } else {
        session.setAttribute("errorMessage", "L'indirizzo mail non è corretto");
        response.sendRedirect("/");
      }
    } catch (SQLException ex) {
      session.setAttribute("errorMessage", "Errore di accesso al DB");
      response.sendRedirect("/");
    }
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Servlet per il login del medico di base";
  }// </editor-fold>
}