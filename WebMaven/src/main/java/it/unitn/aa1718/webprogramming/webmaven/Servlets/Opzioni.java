package it.unitn.aa1718.webprogramming.webmaven.Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 *
 * @author andreabalasso
 */
public class Opzioni extends HttpServlet{
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
  */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
      HttpSession session = request.getSession();
      if(session.getAttribute("ruolo")==null)
        request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
      else {
        if(session.getAttribute("ruolo").equals("Paziente"))
          request.getRequestDispatcher("opzioniP.jsp").forward(request, response);  
        else if(session.getAttribute("ruolo").equals("Medico di Base"))
          request.getRequestDispatcher("opzioniMB.jsp").forward(request, response);
        else if(session.getAttribute("ruolo").equals("Medico Specialista"))
          request.getRequestDispatcher("opzioniMS.jsp").forward(request, response);
        else
          request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);  //in caso il ruolo non sia riconosciuto
      }
    }
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Servlet che permette all'Utente di accedere alle proprie opzioni";
  }// </editor-fold>
}