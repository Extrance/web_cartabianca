package it.unitn.aa1718.webprogramming.webmaven.Servlets;

import com.google.zxing.WriterException;
import com.itextpdf.text.DocumentException;
import it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database;
import it.unitn.aa1718.webprogramming.webmaven.GeneratePDF;
import it.unitn.aa1718.webprogramming.webmaven.QRGenerator;
import it.unitn.aa1718.webprogramming.webmaven.SendMail;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.mail.EmailException;
/**
 *
 * @author andreabalasso
 */
public class NuovaRicetta extends HttpServlet {
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
      HttpSession session = request.getSession();
      if(session.getAttribute("username")==null || !session.getAttribute("ruolo").equals("Medico di Base"))
        request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
      else
        request.getRequestDispatcher("nuovaricetta.jsp").forward(request, response);
    }
  }
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   * @throws java.io.FileNotFoundException
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, FileNotFoundException {
    HttpSession session = request.getSession();
    if(session.getAttribute("username")==null || !session.getAttribute("ruolo").equals("Medico di Base"))
      request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
    else {
      try {
        String paziente = (String) request.getParameter("paziente");
        String email = "";
        String descrizione = "";
        if(request.getParameter("descrizione")!=null)
          descrizione = (String) request.getParameter("descrizione");
      
        descrizione = descrizione.replace("'", "`");
        descrizione = descrizione.replace("\"", "``");
        
        java.util.Date utilDate = new java.util.Date();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        Date data = sqlDate;

        Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
        Connection conn = db.getConnection();

        PreparedStatement st1;
        st1 = conn.prepareStatement("SELECT P.email FROM Pazienti AS P WHERE P.email=? OR P.codice_fiscale=?");
        st1.setString(1, paziente);
        st1.setString(2, paziente);     
        ResultSet rs1 = st1.executeQuery();
        if(rs1.next()) {
          email = rs1.getString(1);
          if(rs1.next()) {  //Impossibile, ma non si sa mai
            session.setAttribute("errorMessage","Entry duplicata per la stessa email");
            throw new SQLException("Entry duplicata per la stessa email");
          }
        } else {
          session.setAttribute("errorMessage","Nessuna corrispondenza trovata");
          throw new SQLException("Nessuna corrispondenza trovata");
        }
             
        PreparedStatement st2;
        st2 = conn.prepareStatement("INSERT INTO RicTot (id_medico,id_paziente,descrizione,data) VALUES (?,?,?,?)");
        st2.setString(1, (String) session.getAttribute("username"));
        st2.setString(2, email);
        st2.setString(3, descrizione);
        st2.setDate(4, data);
        int rs2 = st2.executeUpdate();
        int id= -1;
        PreparedStatement st3;
        st3 = conn.prepareStatement("SELECT id FROM RicTot WHERE id_medico=? AND descrizione=? AND data=? AND erogata=false ORDER BY id DESC LIMIT 1");
        st3.setString(1, (String) session.getAttribute("username"));
        st3.setString(2, descrizione);
        st3.setDate(3, data);
        ResultSet rs3 = st3.executeQuery();
        if(rs3.next()) {
          id = rs3.getInt(1);
          if(rs3.next()) {  //Impossibile, ma non si sa mai
            session.setAttribute("errorMessage","Entry duplicata");
            throw new SQLException("Entry duplicata");
          }
        } else {
          session.setAttribute("errorMessage","Nessuna corrispondenza trovata");
          throw new SQLException("Nessuna corrispondenza trovata");
        }

        PreparedStatement st4;
        st4 = conn.prepareStatement("SELECT id FROM Farmaci");
        ResultSet rs4 = st4.executeQuery();
        while(rs4.next()) {
          if(request.getParameter(""+rs4.getInt(1)+"")!=null && request.getParameter(""+rs4.getInt(1)+"")!="") {
            PreparedStatement stTMP;
            stTMP = conn.prepareStatement("INSERT INTO Ricette2 (id_ric,id_farmaco,quantita) VALUES (?,?,?)");
            stTMP.setInt(1, id);
            stTMP.setInt(2, rs4.getInt(1));
            //System.out.println("Ind: "+request.getParameter(""+rs4.getInt(1)+""));
            stTMP.setInt(3, (Integer.parseInt(request.getParameter(""+rs4.getInt(1)))));
            int rsTMP = stTMP.executeUpdate();
          }
        }
        //Notify via email
        SendMail.nuovaRicettaMail(email);
        //Retrieve path for website media
        String pathAppoggio = this.getClass().getClassLoader().getResource("").getPath();
        String fullPath = URLDecoder.decode(pathAppoggio, "UTF-8");
        String pathArr[] = fullPath.split("target/");
        fullPath = pathArr[0];
        fullPath = fullPath + "src/main/privateImg/";
        //Create QRCode
        String content = "id_ricetta:"+id+"-id_paziente:"+email+"-id_medico:"+(session.getAttribute("matricola"))+"-data:"+data+"";
        
        PreparedStatement st5;
        st5 = conn.prepareStatement("SELECT id_farmaco, quantita FROM Ricette2 WHERE id_ric=?");
        st5.setInt(1, id);
        ResultSet rs5 = st5.executeQuery();
        while(rs5.next()) {
          content = content+"-id_farmaco: "+rs5.getInt(1)+"-quantita: "+rs5.getInt(2)+"";
        }
        
        QRGenerator.generateQRCodeImage(content, id, fullPath);    
        GeneratePDF.generatePDFFun(id,fullPath);
        
        session.setAttribute("esitoInserimento","Ricetta creata correttamente");
        response.sendRedirect("MedicoBasePage");
      }catch (IOException | SQLException | EmailException | WriterException | DocumentException e) {
        if(session.getAttribute("errorMessage")==null)
          session.setAttribute("errorMessage","Errore");
        response.sendRedirect("NuovaRicetta");
      }
    }
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Questa servlet gestisce la creazione di una nuova RICETTA da parte di un Medico di Base";
  }// </editor-fold>
}