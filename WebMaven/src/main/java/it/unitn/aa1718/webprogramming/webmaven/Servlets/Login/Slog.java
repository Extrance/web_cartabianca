package it.unitn.aa1718.webprogramming.webmaven.Servlets.Login;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 *
 * @author andreabalasso
 */
public class Slog extends HttpServlet{
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();
    response.sendRedirect("/");
  }
  /**
   *
   * @param request
   * @param response
   * @throws IOException
   * @throws javax.servlet.ServletException
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
    String caller = request.getParameter("caller");
    switch(caller){
      case "LogOut":
        HttpSession session = request.getSession();
        session.setAttribute("username", null);
        session.setAttribute("ruolo", null);
        session.setAttribute("nome", null);
        session.setAttribute("cognome", null);
        session.setAttribute("sesso", null);
        session.setAttribute("data_nascita", null);
        session.setAttribute("luogo_nascita", null);
        session.setAttribute("cf", null);
        session.setAttribute("foto", null);
        session.setAttribute("id_medico", null);
        session.setAttribute("provincia", null);
        session.setAttribute("matricola", null);
        session.setAttribute("errorMessage", null);
        break;
      default:
        break;
    }
    response.sendRedirect("/");
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Servlet che permette all'Utente di effettuare il logout";
  }// </editor-fold>
}