package it.unitn.aa1718.webprogramming.webmaven.Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 *
 * @author andreabalasso
 */
public class PazientePage extends HttpServlet{
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    HttpSession session = request.getSession();
    if(session.getAttribute("username")==null || !session.getAttribute("ruolo").equals("Paziente"))
      request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);   
    else
      request.getRequestDispatcher("paziente.jsp").forward(request, response);   
  }
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Servlet per la pagina personale del Paziente";
  }// </editor-fold>
}