package it.unitn.aa1718.webprogramming.webmaven;
/**
 *
 * @author andreabalasso
 */
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

public class SendMail {

  public static final String HOST_NAME = "smtp.gmail.com";
  public static final int PORT = 465;
  public static final String TEXT_PLAIN = "text/plain";
  
  public static final String ADMIN = "servizisanitari.project@gmail.com";
  public static final String PASSWORD = "ServiziSanitariCartaBianca";

  public static void changePasswordMail(String mail,String newPw) throws IOException, EmailException {

    final String recipientEmailAddress = mail;  //set mail of the receiver

    HtmlEmail email = new HtmlEmail();
    email.setHostName(HOST_NAME);
    email.setSmtpPort(PORT);
    email.setSSLOnConnect(true);

    email.setAuthentication(ADMIN, PASSWORD);

    email.setSubject("Servizi Sanitari - Change Password");
    email.setFrom(ADMIN, "ServiziSanitari", String.valueOf(StandardCharsets.UTF_8));
    email.addTo(recipientEmailAddress);
    email.setHtmlMsg("<h3>Servizi Sanitari - Change Password<br></h3>"
      + "Utente: " + mail + "<br>"
      + "Nuova Password: " + newPw + "<br>");
        
    email.send();
  }
    
  public static void resetPasswordMail(String mail, String newPw) throws EmailException {

    final String recipientEmailAddress = mail;                          //set mail of the receiver

    HtmlEmail email = new HtmlEmail();
    email.setHostName(HOST_NAME);
    email.setSmtpPort(PORT);
    email.setSSLOnConnect(true);

    email.setAuthentication(ADMIN, PASSWORD);

    email.setSubject("Servizi Sanitari - Reset Password");
    email.setFrom(ADMIN, "ServiziSanitari", String.valueOf(StandardCharsets.UTF_8));
    email.addTo(recipientEmailAddress);
    email.setHtmlMsg("<h3>Servizi Sanitari - Reset Password<br></h3>"
      + "Utente: " + mail + "<br>"
      + "Nuova Password: " + newPw + "<br>");

    email.send();
  }
    
  public static void changeMedicoMail(String mail, String nome, String cognome, String id) throws EmailException {

    final String recipientEmailAddress = mail;  //set mail of the receiver

    HtmlEmail email = new HtmlEmail();
    email.setHostName(HOST_NAME);
    email.setSmtpPort(PORT);
    email.setSSLOnConnect(true);

    email.setAuthentication(ADMIN, PASSWORD);

    email.setSubject("Servizi Sanitari - Cambio Medico di Base");
    email.setFrom(ADMIN, "ServiziSanitari", String.valueOf(StandardCharsets.UTF_8));
    email.addTo(recipientEmailAddress);
    email.setHtmlMsg("<h3>Servizi Sanitari - Cambio Medico di Base<br></h3>"
      + "Utente: " + mail + "<br>"
      + "Nuovo Medico di Base: " + nome + " "+ cognome +" ("+ id +")<br>");
    
    email.send();
  }
    
  public static void nuovaVisitaMail(String mail) throws EmailException {

    final String recipientEmailAddress = mail;  //set mail of the receiver

    HtmlEmail email = new HtmlEmail();
    email.setHostName(HOST_NAME);
    email.setSmtpPort(PORT);
    email.setSSLOnConnect(true);

    email.setAuthentication(ADMIN, PASSWORD);

    email.setSubject("Servizi Sanitari - Nuova Visita");
    email.setFrom(ADMIN, "ServiziSanitari", String.valueOf(StandardCharsets.UTF_8));
    email.addTo(recipientEmailAddress);
    email.setHtmlMsg("<h3>Servizi Sanitari - Nuova Visita<br></h3>"
      + "Nuova visita, si prega di accedere alla propria area personale per ulteriori dettagli.<br>");
    
    email.send();
  }
    
  public static void nuovaRicettaMail(String mail) throws EmailException {

    final String recipientEmailAddress = mail;  //set mail of the receiver

    HtmlEmail email = new HtmlEmail();
    email.setHostName(HOST_NAME);
    email.setSmtpPort(PORT);
    email.setSSLOnConnect(true);

    email.setAuthentication(ADMIN, PASSWORD);

    email.setSubject("Servizi Sanitari - Nuova Ricetta");
    email.setFrom(ADMIN, "ServiziSanitari", String.valueOf(StandardCharsets.UTF_8));
    email.addTo(recipientEmailAddress);
    email.setHtmlMsg("<h3>Servizi Sanitari - Nuova Ricetta<br></h3>"
      + "Nuova ricetta, si prega di accedere alla propria area personale per ulteriori dettagli<br>");
        
    email.send();
  }
    
  public static void nuovaPrescrizioneMail(String mail) throws EmailException {

    final String recipientEmailAddress = mail;  //set mail of the receiver

    HtmlEmail email = new HtmlEmail();
    email.setHostName(HOST_NAME);
    email.setSmtpPort(PORT);
    email.setSSLOnConnect(true);

    email.setAuthentication(ADMIN, PASSWORD);

    email.setSubject("Servizi Sanitari - Nuova Prescrizione");
    email.setFrom(ADMIN, "ServiziSanitari", String.valueOf(StandardCharsets.UTF_8));
    email.addTo(recipientEmailAddress);
    email.setHtmlMsg("<h3>Servizi Sanitari - Nuova Prescrizione<br></h3>"
      + "Nuova prescrizione, si prega di accedere alla propria area personale per ulteriori dettagli<br>");
    
    email.send();
  }
    
  public static void nuovoEsitoMail(String mail) throws EmailException {

    final String recipientEmailAddress = mail;  //set mail of the receiver

    HtmlEmail email = new HtmlEmail();
    email.setHostName(HOST_NAME);
    email.setSmtpPort(PORT);
    email.setSSLOnConnect(true);

    email.setAuthentication(ADMIN, PASSWORD);

    email.setSubject("Servizi Sanitari - Nuovo Esito");
    email.setFrom(ADMIN, "ServiziSanitari", String.valueOf(StandardCharsets.UTF_8));
    email.addTo(recipientEmailAddress);
    email.setHtmlMsg("<h3>Servizi Sanitari - Nuovo Esito<br></h3>"
      + "Nuovo esito, si prega di accedere alla propria area personale per ulteriori dettagli<br>");
        
    email.send();
  }
    
  public static void scaricaRicetta(String mail, String path) throws EmailException, MalformedURLException {

    final String recipientEmailAddress = mail;  //set mail of the receiver

    HtmlEmail email = new HtmlEmail();
    email.setHostName(HOST_NAME);
    email.setSmtpPort(PORT);
    email.setSSLOnConnect(true);

    email.setAuthentication(ADMIN, PASSWORD);

    email.setSubject("Servizi Sanitari - Ricetta da stampare");
    email.setFrom(ADMIN, "ServiziSanitari", String.valueOf(StandardCharsets.UTF_8));
    email.addTo(recipientEmailAddress);
    email.setHtmlMsg("<h3>Servizi Sanitari - Ricetta da stampare<br></h3>"
      + "In allegato la richiesta da Lei desiderata. Puo' far riferimento alla propria farmacia di fiducia per l'erogazione dei farmaci di cui ha bisogno.");
        
    //Attach file to email
    File pdf = new File(path);
    email.embed(pdf.toURI().toURL(), pdf.getName());
        
    email.send();
  }
  
  public static void scaricaTicket(String mail, String path) throws EmailException, MalformedURLException {

    final String recipientEmailAddress = mail;  //set mail of the receiver

    HtmlEmail email = new HtmlEmail();
    email.setHostName(HOST_NAME);
    email.setSmtpPort(PORT);
    email.setSSLOnConnect(true);

    email.setAuthentication(ADMIN, PASSWORD);

    email.setSubject("Servizi Sanitari - Report Ticket");
    email.setFrom(ADMIN, "ServiziSanitari", String.valueOf(StandardCharsets.UTF_8));
    email.addTo(recipientEmailAddress);
    email.setHtmlMsg("<h3>Servizi Sanitari - Report Ticket<br></h3>"
      + "In allegato il report dei suoi ticket.");
        
    //Attach file to email
    File pdf = new File(path);
    email.embed(pdf.toURI().toURL(), pdf.getName());
        
    email.send();
  }
}