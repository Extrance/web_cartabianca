package it.unitn.aa1718.webprogramming.webmaven.Servlets;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DownloadServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      String contentType = "application/octet-stream";
      String fileName ="";
      fileName = (String) request.getAttribute("fileName");
      byte[] file = getFileOnServer(fileName);

      response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
      response.setHeader("charset", "iso-8859-1");
      response.setContentType(contentType);
      response.setContentLength(file.length);
      response.setStatus(HttpServletResponse.SC_OK);

      OutputStream outputStream = null;
      try {
          outputStream = response.getOutputStream();
          outputStream.write(file, 0, file.length);
          outputStream.flush();
          outputStream.close();
          response.flushBuffer();
      } catch (IOException e) {
          throw new RuntimeException(e);
      }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      doPost(request, response);
    }

    private byte[] getFileOnServer(String fileName) throws UnsupportedEncodingException, FileNotFoundException, IOException {
      String pathAppoggio = this.getClass().getClassLoader().getResource("").getPath();
      String fullPath = URLDecoder.decode(pathAppoggio, "UTF-8");
      String pathArr[] = fullPath.split("target/");
      fullPath = pathArr[0];
      fullPath = fullPath + "src/main/privateImg/report/";
      File file = new File(fullPath+fileName+".xlsx");
      //implement your method to get the file in byte[]
      byte[] bytesArray = new byte[(int) file.length()]; 
      
      FileInputStream fis = new FileInputStream(file);
      fis.read(bytesArray); //read file into bytes[]
      fis.close();
			
      return bytesArray;
    }
}