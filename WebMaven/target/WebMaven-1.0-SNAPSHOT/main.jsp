<%-- 
    Document   : main
    Created on : 17-ott-2019, 18.40.39
    Author     : andreabalasso
--%>

<%@page import="java.time.LocalDate"%>
<%@page import="java.time.ZoneId"%>
<%@page import="java.util.Date"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    
    <title>Servizi sanitari</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"></link>
    <link rel = "icon" href ="img/transparent.png" type = "image/png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="css/sidenav.css" type="text/css"/>
    <link rel="stylesheet" href="css/autocompleteCss.css" type="text/css"/>
    <script type="text/javascript" src="js/autocompleteFile.js"></script>
    <style>
      body {
        background-image: url(img/background.jpg);
        background-position: top;
        background-attachment: fixed;
        background-repeat: no-repeat;
        position: relative;
      }
    </style>
  </head>
  <body onload="funP(); funMB(); funMS();" data-spy="scroll" data-target=".navbar" data-offset="50">
    
    <!--Navigation bar-->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href=""><img src="img/transparent.png" width="40px" style="margin-top: -9.5px; "></a>
        </div>
        <div class="collapse navbar-collapse"  style="padding-left:30px;" id="myNavbar">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#section1">Chi siamo</a></li>
            <li><a href="#section2">Servizi</a></li>
            <li><a href="#section3">Dove trovarci</a></li>
            <%  
              if(session.getAttribute("ruolo")!=null) {
                if(session.getAttribute("ruolo").equals("Paziente")) {
            %>
                  <li>
                    <form class="navbar-form navbar-left" style="max-width: 250px;" action="CercaEsame" method="POST">  <!--DA FARE-->
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="Cerca esame" name="esame">
                        <div class="input-group-btn">
                          <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                  </li>
                <% } else if(session.getAttribute("ruolo").equals("Medico di Base") || session.getAttribute("ruolo").equals("Medico Specialista")) { %>
                  <li>
                    <form class="navbar-form navbar-left" autocomplete="off" style="max-width: 250px;" action="RefertoPaziente" method="POST">
                      <div class="input-group autocomplete">
                        <input id="myInput" type="text" class="form-control" placeholder="Cerca paziente" name="paziente">
                        <div class="input-group-btn">
                          <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                  </li>
                <% } %>
            <% } %>
          </ul>
          <% if(session.getAttribute("username")==null) { %>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a>
                <ul class="dropdown-menu">
                  <li><a href="#" data-toggle="modal" data-target="#loginP">Paziente</a></li>
                  <li><a href="#" data-toggle="modal" data-target="#loginMB">Medico di Base</a></li>
                  <li><a href="#" data-toggle="modal" data-target="#loginMS">Medico Specialista</a></li>
                </ul>
              </li>
            </ul>
          <% } else { %>
          <div class="nav navbar-nav navbar-right" style="padding-right:10px;">
            <ul class="nav navbar-nav navbar-right">
              <li>
                <%if(session.getAttribute("ruolo").equals("Paziente")) { %>
                  <form action="PazientePage" method="GET">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="Paziente">
                      <span class="glyphicon glyphicon-user"></span>
                    </button>
                  </form>
                <% } else if(session.getAttribute("ruolo").equals("Medico di Base")) {%>
                  <form action="MedicoBasePage" method="GET">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="Medico di Base">
                      <span class="glyphicon glyphicon-user"></span>
                    </button>
                  </form>
                <% } else if(session.getAttribute("ruolo").equals("Medico Specialista")) {%>
                  <form action="MedicoSpecPage" method="GET">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="Medico Specialista">
                      <span class="glyphicon glyphicon-user"></span>
                    </button>
                  </form>
                <% } %>
              </li>
              <li>
                <form action="Slog" method="POST">
                  <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="LogOut">
                    <span class="glyphicon glyphicon-log-out"></span> Logout
                  </button>
                </form>
              </li>
            </ul>
          </div>
          <% } %>
        </div>
      </div>
    </nav>
    
    <%  if(session.getAttribute("errorMessage")!=null) {
          session.setAttribute("errorMessage", null);
    %>
      <script>
        window.alert("Errore in fase di elaborazione della richiesta");
      </script>
    <%
        }
    %>

    <!--Form Login Paziente-->
    <div class="modal fade" id="loginP" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <form method="POST" action="LoginPaziente" name="TestFormP">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Login Paziente</h4>
            </div>
            <div class="modal-body">
              <input type="text" placeholder="Email" name="username" style="width: 250px;"><br><br>
              <input type="password" placeholder="Password" name="password" style="width: 250px;"><br>
              <input type="checkbox" name="rememberme" value="rememberme"> Ricordami<br><br>
              <a href="ForgotPassword" style="color: hotpink;">Password Dimenticata</a>
              <script type="text/javascript">
                function funP() {
                  if (document.cookie != ""){
                    cookies = document.cookie.split(";");
                    for (var i = 0; i < cookies.length; i++) {
                      cookie = cookies[i].trim().split("=");
                      if (cookie[0] == "usernamePaziente") {
                        cookie = cookies[i].trim().split("\"");
                        document.TestFormP.username.value = cookie[1];
                      }
                      if (cookie[0] == "passwordPaziente") {
                        document.TestFormP.password.value = cookie[1];
                      }
                    }
                  }
                }    
              </script>
            </div>
            <div class="modal-footer">
              <input type="submit" class="btn btn-primary" value="Accedi"><br>
            </div>
          </form>
        </div>
      </div>
    </div>
    
    <!--Form Login Medico di Base-->
    <div class="modal fade" id="loginMB" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <form method="POST" action="LoginMedicoBase" name="TestFormMB">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Login Medico di Base</h4>
            </div>
            <div class="modal-body">
              <input type="text" placeholder="Email" name="username" style="width: 250px;"><br><br>
              <input type="password" placeholder="Password" name="password" style="width: 250px;"><br>
              <input type="checkbox" name="rememberme" value="rememberme"> Ricordami<br><br>
              <a href="ForgotPassword" style="color: hotpink;">Password Dimenticata</a>
              <script type="text/javascript">                 
                function funMB() {
                    if (document.cookie != ""){
                        cookies = document.cookie.split(";");
                         for (var i = 0; i < cookies.length; i++) {
                            cookie = cookies[i].trim().split("=");
                            if (cookie[0] == "usernameMB") {
                                cookie = cookies[i].trim().split("\"");
                                document.TestFormMB.username.value = cookie[1];
                            }
                            if (cookie[0] == "passwordMB") {
                                document.TestFormMB.password.value = cookie[1];
                            }
                        }
                    }
                }    
              </script>
            </div>
            <div class="modal-footer">
              <input type="submit" class="btn btn-primary" value="Accedi"><br>
            </div>
          </form>
        </div>
      </div>
    </div>
    
    <!--Form Login Medico Specialista-->
    <div class="modal fade" id="loginMS" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <form method="POST" action="LoginMedicoSpecialista" name="TestFormMS">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Login Medico Specialista</h4>
            </div>
            <div class="modal-body">
              <input type="text" placeholder="Email" name="username" style="width: 250px;"><br><br>
              <input type="password" placeholder="Password" name="password" style="width: 250px;"><br>
              <input type="checkbox" name="rememberme" value="rememberme"> Ricordami<br><br>
              <a href="ForgotPassword" style="color: hotpink;">Password Dimenticata</a>
              <script type="text/javascript">                 
                function funMS() {
                    if (document.cookie != ""){
                        cookies = document.cookie.split(";");
                        for (var i = 0; i < cookies.length; i++) {
                            cookie = cookies[i].trim().split("=");
                            if (cookie[0] == "usernameMS") {
                                cookie = cookies[i].trim().split("\"");
                                document.TestFormMS.username.value = cookie[1];
                            }
                            if (cookie[0] == "passwordMS") {
                                document.TestFormMS.password.value = cookie[1];
                            }
                        }
                    }
                }    
              </script>
            </div>
            <div class="modal-footer">
              <input type="submit" class="btn btn-primary" value="Accedi"><br>
            </div>
          </form>
        </div>
      </div>
    </div>
    
    <!--Page Content-->
    <div class="container-fluid">
      <div class="row">
      <div class="col-sm-1">
      </div>
      <div class="col-sm-7">  
        <div id="section1" class="container-fluid" style="padding-top:50px; text-align: justify;">
          <h1>Chi siamo</h1>
          Questo sito nasce per essere una piattaforma ideale per la gestione delle procedure
          inerenti all'ambito sanitario in un contesto provinciale.<br>
          L'obiettivo e' quello di fornire uno spazio facile da usare e immediato, sia al paziente
          sia ai medici.<br><br>
          Questo sito vuole quindi essere quanto piu' accessibile a ogni paziente,
          fornendogli tutto cio' di cui ha bisogno per preservare la propria salute, ma anche ad ogni
          medico, fornendogli tutte le procedure necessarie al compimento del proprio lavoro.
        </div>
        <div id="section2" class="container-fluid" style="padding-top:30px; text-align: justify;">
          <hr>
          <h1>Servizi</h1>
          Su questo sito ogni paziente potra' avere a disposizione tutti gli strumenti necessari alla
          gestione della propria storia clinica presente, passata e futura!<br>
          Ogni paziente puo' gestire autonomamente la propria area personale, gestire le proprie ricette,
          avere a portata di mano gli esiti di tutte le sue visite e esami e controllare le proprie
          spese mediche, il tutto in maniera rapida, semplice ed efficace!<br><br>
          Al nostro personale medico invece sono forniti strumenti atti all'espletamento puntuale della
          loro attivita' lavorativa. L'obiettivo primario e' fornire ad ogni medico la piattaforma ideale
          per informatizzare i dati relativi alle varie procedure mediche.<br><br>
          Ai medici di base e' quindi possibile in pochi semplici click informatizzare ogni dato relativo
          alle visite mediche fatte, prescrivere esami o farmaci.<br><br>
          Ai medici specialisti e' invece possibile gestire l'erogazione degli esami ai quali i pazienti
          si sottopongono, avendo sempre a disposizione la storia clinica del paziente, in modo da avere
          anamnesi sempre piu' precise e dettagliate.
        </div>
        <div id="section3" class="container-fluid" style="padding-top:30px; text-align: justify;">
          <hr>
          <h1>Dove trovarci</h1>
          Dove trovarci? Su questo sito ovviamente!<br>
          Se le procedure che richiedono interazione tra il paziente e il medico sono effettuate nelle
          strutture sanitarie adibite a questa gestione, tramite questo portale sanitario sara' sempre
          piu' semplice gestire da casa propria la stampa delle proprie ricette, vedere gli esiti
          relativi alle proprie visite e esami.<br><br>
          Al momento le strutture convenzionate sono tutte quelle delle province di Vicenza e Padova,
          se sei un cittadino di queste province hai gia' ricevuto le credenziali per l'accesso al sistema.<br>
          Procedi subito al Login per rivoluzionare la tua esperienza con i servizi sanitari informatizzati!<br><br>
          Se ancora non hai ricevuto i dati richiesti, puoi trovare di seguito i nostri contatti.<br>
          Non esitare a contattarci, ti guideremo nel setup delle tue credenziali di accesso.  
        </div>
      </div>
      <div class="col-sm-4" >
      </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-7">
          <div class="container-fluid" style="padding-top:50px; padding-bottom:100px; text-align: right;">
          <hr>
          <h1>Contatti</h1>
          <p>+39 333 3152405 <span class="glyphicon glyphicon-earphone hidden-xs"></p>
          <p>servizisanitari.project@gmail.com <span class="glyphicon glyphicon-envelope hidden-xs"></p>
          <p>Via Sommarive 9 - Trento (TN) <span class="glyphicon glyphicon-inbox hidden-xs"></p>
        </div>
        </div>
        <div class="col-sm-1">
        </div>
      </div>
    </div>
    
    <nav class="navbar navbar-inverse navbar-fixed-bottom">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">Servizi Sanitari</a>
        </div>
      </div>
    </nav>
    
    <!--Script per creare degli array di appoggio utili alla suggestion box-->
    <script>
      var nomi = [];
      var cognomi = [];
      var email = [];
      var cf = [];
      var citta = [];
      <%
        try {
          Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
          Connection conn = db.getConnection();
          PreparedStatement st = conn.prepareStatement("SELECT nome, cognome, email, codice_fiscale, luogo_nascita FROM Pazienti WHERE id_medico = ?");
          st.setString(1, ((String) session.getAttribute("username")));
          ResultSet rs = st.executeQuery();
          while(rs.next()) {
      %>
        nomi.push("<%=rs.getString(1)%>");
        cognomi.push("<%=rs.getString(2)%>");
        email.push("<%=rs.getString(3)%>");
        cf.push("<%=rs.getString(4)%>");
        citta.push("<%=rs.getString(5)%>");
      <%
          }
        } catch(Exception e) {
          response.sendRedirect("/");
        }
      %>
      autocomplete(document.getElementById("myInput"), nomi,cognomi,email,cf,citta);
    </script>
    
    
    <script type="text/javascript" id="cookieinfo" src="//cookieinfoscript.com/js/cookieinfo.min.js"></script>


  </body>
</html>