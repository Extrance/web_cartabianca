<%-- 
    Document   : ticketpaziente
    Created on : 16-dic-2019, 12.09.46
    Author     : andreabalasso
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.PazienteDAO"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.DAOCreate"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Paziente"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Servizi sanitari - Ticket</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel = "icon" href ="img/transparent.png" type = "image/png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="css/sidenav.css" type="text/css"/>
    <link rel="stylesheet" href="css/infoCustom.css" type="text/css"/>
    <style>
      body{
        background-image: url(img/background.jpg);
        background-position: top;
        background-attachment: fixed;
        background-repeat: no-repeat;
      }
    
      .btn-link:hover {
        opacity: 0.5;
      }
      
      @media screen and (min-width: 1050px) {
        .customInline {
           display: inline-block;
        }
      }
    </style>
    <script type="text/javascript">
      function infoEsame(id, referto) {
        alert("Esame "+id+"\n"+referto);
      }
    </script>
  </head>
  <body>
    
    <%  if(session.getAttribute("errorMessage")!=null) {
          session.setAttribute("errorMessage", null);
    %>
          <script>window.alert("Errore in fase di elaborazione della richiesta");</script>
    <%
        } else if(session.getAttribute("stampaTicket")!=null) {
            session.setAttribute("stampaTicket", null);
    %>
            <script>window.alert("Report ticket generato correttamente, verifica la tua mailbox");</script>
    <%
        }
    %>
    
    <% if(session.getAttribute("ruolo")!=null && session.getAttribute("ruolo").equals("Paziente")) { %>
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="img/transparent.png" width="40px" style="margin-top: -9.5px; "></a>
          </div>
          <div class="collapse navbar-collapse"  style="padding-left:30px;" id="myNavbar">
            <ul class="nav navbar-nav">
              <li><a href="/">Chi siamo</a></li>
              <li><a href="/#section2">Servizi</a></li>
              <li><a href="/#section3">Dove trovarci</a></li>
              <li>
                <form class="navbar-form navbar-left" style="max-width: 250px;" action="CercaEsame" method="POST">  <!--DA FARE-->
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Cerca esame" name="esame">
                    <div class="input-group-btn">
                      <button class="btn btn-default" type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </li>
            </ul>
            <div class="nav navbar-nav navbar-right" style="padding-right:10px;">
              <ul class="nav navbar-nav navbar-right">
                <li>
                  <form action="PazientePage" method="GET">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="Paziente">
                      <span class="glyphicon glyphicon-user"></span>
                    </button>
                  </form>
                </li>
                <li>
                  <form action="Slog" method="POST">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="LogOut">
                      <span class="glyphicon glyphicon-log-out"></span> Logout
                    </button>
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    <% } %>
    
    <div class="container-fluid text-center" style="margin-top:50px">  
      <%
        try {
          PazienteDAO utd = DAOCreate.getUtenteDAO();
          Paziente req = utd.getUtenteByEmail((String) session.getAttribute("username"));
        
          if (req==null) {    //no match found
      %>
        <div class="col-sm-12 text-left"> 
          <h1>Attenzione: Login come paziente non effettuato</h1>
          <p>Si prega di effettuare il login per accedere al contenuto di questa pagina</p>
          <a href="main.jsp" class="btn btn-link">Menu Principale</a>
        </div>
      <% } else { %>
        <div class="row content">
          <div class="col-sm-2 sidenav" style="font-family: sans-serif;">
            <div style="text-align: center;">
              <br>
              <p><a href="EsamiPaziente" style="text-decoration: none;"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Esami</a></p>            <!--style="font-weight: bold;"-->
              <p><a href="RicettePaziente" style="text-decoration: none;"><span class="glyphicon glyphicon-duplicate"></span>&nbsp;&nbsp;Ricette</a></p>           <!--style="text-decoration: underline;"--> 
              <p><a href="VisitePaziente" style="text-decoration: none;"><span class="glyphicon glyphicon-calendar"></span>&nbsp;&nbsp;Visite</a></p>
              <p><a href="#" style="color: blue; text-decoration: none;"><span class="glyphicon glyphicon-credit-card"></span>&nbsp;&nbsp;Ticket</a></p>
              <hr>
              <p><a href="Opzioni" style="text-decoration: none;"><span class="glyphicon glyphicon-cog"></span>&nbsp;&nbsp;Opzioni</a></p>                               <!--style="color: red;"-->
            </div>
          </div>
          <div class="col-sm-10 text-left contentView">
            <div class="row">
              <div class="col-sm-3 text-center" style="padding: 30px;">
                <%
                  Database db2 = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
                  Connection conn2 = db2.getConnection();
                  PreparedStatement st2 = conn2.prepareStatement("SELECT foto FROM Pazienti WHERE codice_fiscale= ?");
                  st2.setString(1, (String) session.getAttribute("cf"));
                  ResultSet rs2 = st2.executeQuery();
                  while(rs2.next()) {
                %>
                  <img src="img/users/<%= rs2.getString(1) %>.jpg" alt="Profile picture" style="width:128px;height:128px;">
                <%
                  }
                %>
              </div>
              <div class="col-sm-9 text-left">
                <h1><%= session.getAttribute("cognome")%> <%= session.getAttribute("nome")%></h1>
                <p style="padding-left: 10px;">
                  <span style="color: blue;">Mail</span>&nbsp;<%= session.getAttribute("username")%><br>
                  <span style="color: blue;">CF</span>&nbsp;&nbsp;&nbsp;<%=session.getAttribute("cf")%>
                </p>
                <hr>
                <p style="padding-left: 10px;"><%= session.getAttribute("data_nascita")%> <%= session.getAttribute("luogo_nascita")%></p>
              </div>
            </div>
            <div class="row" style="padding: 20px;">
              <hr>
              <b>Medico di base attuale:</b>
              <%
                Database db = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
                Connection conn = db.getConnection();
                PreparedStatement st = conn.prepareStatement("SELECT DISTINCT M.email, M.nome, M.cognome FROM MediciB AS M, Pazienti AS P WHERE P.id_medico= ? AND M.email=P.id_medico");
                st.setString(1, (String) session.getAttribute("id_medico"));
                ResultSet rs = st.executeQuery();
                while(rs.next()) {
              %>
                <%= rs.getString(2)%> <%= rs.getString(3)%> - (<%= rs.getString(1)%>)
              <%
                }
              %>
              <br><hr>
              <form action="TicketPaziente" method="POST">
                <img src="img/PDF.png" style="height: 30px;"><input type="submit" class="btn btn-link" style="color:red;" value="Scarica Report">
              </form>
              <hr>
              
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#pagati" style="color: green;">Pagati</a></li>
                <li><a data-toggle="tab" href="#dapagare" style="color: red;">Da Pagare</a></li>
              </ul>
              <br>
              <div class="tab-content">
                <div id="pagati" class="tab-pane fade in active" style="margin-left: 10px;">
                  <table class="table table-bordered table-striped" style="width: 80%;">
                    <thead style="background-color: green;">
                      <tr>
                        <th style="text-align: center;">ID</th>
                        <th style="text-align: center;">Esame</th>
                        <th style="text-align: center;">Medico S</th>
                        <th style="text-align: center;">Data</th>
                        <th style="text-align: center;">Info</th>
                      </tr>
                    </thead>
                    <tbody id="myTable" style="text-align: center;">
                      <%
                        Database db3 = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
                        Connection conn3 = db3.getConnection();
                        PreparedStatement st3 = conn3.prepareStatement("SELECT P.id, E.id, E.nome, M.matricola, P.data, P.referto FROM Prescrizioni AS P, Esami AS E, MediciS AS M WHERE M.email=P.id_medicoS AND P.ticket=true AND E.id = P.id_esame AND P.erogato=true AND P.id_paziente = ?");
                        st3.setString(1, ((String) session.getAttribute("username")));
                        ResultSet rs3 = st3.executeQuery();
                        while(rs3.next()) {
                      %>
                        <tr>
                          <td><%= rs3.getInt(1)%></td>
                          <td><%= rs3.getInt(2)%>-<%= rs3.getString(3)%></td>
                          <td><%= rs3.getString(4)%></td>
                          <td><%= rs3.getDate(5)%></td>
                          <td>
                            <button class="Info" onclick="infoEsame('<%= rs3.getInt(1)%>', '<%= rs3.getString(6)%>')"><span class="glyphicon glyphicon-file"></span></button>
                          </td>
                        </tr>
                      <%  
                        }
                      %>
                    </tbody>
                  </table>
                </div>
                    
                <div id="dapagare" class="tab-pane fade" style="margin-left: 10px;">
                  <table class="table table-bordered table-striped" style="width: 80%;">
                    <thead style="background-color: red;">
                      <tr>
                        <th style="text-align: center;">ID</th>
                        <th style="text-align: center;">Esame</th>
                        <th style="text-align: center;">Medico S</th>
                        <th style="text-align: center;">Data</th>
                        <th style="text-align: center;">Info</th>
                      </tr>
                    </thead>
                    <tbody id="myTable" style="text-align: center;">
                      <%
                        PreparedStatement st4 = conn3.prepareStatement("SELECT P.id, E.id, E.nome, M.matricola, P.data, P.referto FROM Prescrizioni AS P, Esami AS E, MediciS AS M WHERE M.email=P.id_medicoS AND P.ticket=false AND E.id=P.id_esame AND P.erogato=true AND P.id_paziente = ?");
                        st4.setString(1, ((String) session.getAttribute("username")));
                        ResultSet rs4 = st4.executeQuery();
                        while(rs4.next()) {
                      %>
                        <tr>
                          <td><%= rs4.getInt(1)%></td>
                          <td><%= rs4.getInt(2)%>-<%= rs4.getString(3)%></td>
                          <td><%= rs4.getString(4)%></td>
                          <td><%= rs4.getDate(5)%></td>
                          <td>
                            <button class="Info" onclick="infoEsame('<%= rs4.getInt(1)%>', '<%= rs4.getString(6)%>')"><span class="glyphicon glyphicon-file"></span></button>
                          </td>
                        </tr>
                      <%  
                        }
                      %>
                    </tbody>
                  </table>
                </div>
              </div>
              <br><br><br>
            </div>
          </div>
        </div>
      <% }
        }
        catch(Exception e) {
          response.sendRedirect("main.jsp");
        }
      %>   
    </div>
    
    <nav class="navbar navbar-inverse navbar-fixed-bottom">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">Servizi Sanitari</a>
        </div>
      </div>
    </nav>
    
  </body>
</html>