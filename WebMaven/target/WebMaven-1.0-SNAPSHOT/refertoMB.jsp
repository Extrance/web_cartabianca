<%-- 
    Document   : refertoMB
    Created on : 9-nov-2019, 1.06.46
    Author     : andreabalasso
--%>
<%@page import="java.sql.Date"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.MedicoB"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.MedicoBDAO"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.PazienteDAO"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.DAO.DAOCreate"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Entities.Paziente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <title>Servizi sanitari - Referto Paziente</title>
    
    <link rel="icon" href="img/transparent.png" type="icon/png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="css/sidenav.css" type="text/css"/>
    <link rel="stylesheet" href="css/autocompleteCss.css" type="text/css"/>
    <script type="text/javascript" src="js/autocompleteFile.js"></script>
    <link rel="stylesheet" href="css/infoCustom.css" type="text/css"/>
    <style>
      
      body{
        background-image: url(img/background.jpg);
        background-position: top;
        background-attachment: fixed;
        background-repeat: no-repeat;
      }
    
      .btn-danger:hover {
        opacity: 0.5;
      }
      .btn-link:hover {
        opacity: 0.5;
      }

      @media screen and (max-width: 768px) {
        .customInline {
           display: inline-block;
        }
      }
      
      @media screen and (min-width: 768px) {
        .customInline-opposite {
           display: inline-block;
        }
      }
    </style>
    <script type="text/javascript">
      function infoVisita(id, referto) {
        alert("Visita "+id+"\n"+referto);
      }
      function infoEsame(id, referto) {
        alert("Esame "+id+"\n"+referto);
      }
      function infoRicetta(id, referto) {
        alert("Ricetta "+id+"\n"+referto);
      }
    </script>
  </head>
  <body>  
    <% if(session.getAttribute("ruolo")!=null && session.getAttribute("ruolo").equals("Medico di Base")) { %>
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="img/transparent.png" width="40px" style="margin-top: -9.5px; "></a>
          </div>
          <div class="collapse navbar-collapse"  style="padding-left:30px;" id="myNavbar">
            <ul class="nav navbar-nav">
              <li><a href="/#section1">Chi siamo</a></li>
              <li><a href="/#section2">Servizi</a></li>
              <li><a href="/#section3">Dove trovarci</a></li>
              <li>
                <form class="navbar-form navbar-left" autocomplete="off" style="max-width: 250px;" action="RefertoPaziente" method="POST">
                  <div class="input-group autocomplete">
                    <input id="myInput" type="text" class="form-control" placeholder="Cerca paziente" name="paziente">
                    <div class="input-group-btn">
                      <button class="btn btn-default" type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </li>
            </ul>
            <div class="nav navbar-nav navbar-right" style="padding-right:10px;">
              <ul class="nav navbar-nav navbar-right">
                <li>
                  <form action="MedicoBasePage" method="GET">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="Medico di Base">
                      <span class="glyphicon glyphicon-user"></span>
                    </button>
                  </form>  
                </li>
                <li>
                  <form action="Slog" method="POST">
                    <button type="submit" class="btn btn-link navbar-btn" style="text-decoration: none" name="caller" value="LogOut">
                      <span class="glyphicon glyphicon-log-out"></span> Logout
                    </button>
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    <% } %>
    
    <%  if(session.getAttribute("errorMessage")!=null) {
          session.setAttribute("errorMessage", null);
    %>
      <script>
        window.alert("Errore in fase di elaborazione della richiesta");
      </script>
    <%
        } else {
          if(session.getAttribute("esitoInserimento")!=null) {
    %>
      <script>
        window.alert("Operazione andata a buon fine: "+"<%= (String) session.getAttribute("esitoInserimento")%>");
      </script>
    <%
            session.setAttribute("esitoInserimento",null);
          }
        }
    %>
    
    <div class="container-fluid text-center" style="margin-top:50px">  
      <%
        try {
          MedicoBDAO utd = DAOCreate.getMedicoBDAO();
          MedicoB req = utd.getUtenteByEmail((String) session.getAttribute("username"));
        
          if (req==null) {    //no match found
      %>
        <div class="col-sm-12 text-left"> 
          <h1>Attenzione: Login come medico di base non effettuato</h1>
          <p>Si prega di effettuare il login per accedere al contenuto di questa pagina</p>
          <a href="/" class="btn btn-link">Menu Principale</a>
        </div>
      <% } else { %>
        <div class="row content">
          <div class="col-sm-2 sidenav" style="font-family: sans-serif;">
            <div style="text-align: center;">
              <br>
              <p><a href="ListaPazienti" style="text-decoration: none;"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;Lista pazienti</a></p>
              <p><a href="NuovoEsame" style="text-decoration: none;"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Prescrivi esame</a></p>     <!-- DA FARE -->
              <p><a href="NuovaVisita" style="text-decoration: none;"><span class="glyphicon glyphicon-calendar"></span>&nbsp;&nbsp;Crea Visita</a></p> 
              <p><a href="NuovaRicetta" style="text-decoration: none;"><span class="glyphicon glyphicon-duplicate"></span>&nbsp;&nbsp;Crea Ricetta</a></p>
              <hr>  
              <p><a href="Opzioni" style="text-decoration: none;"><span class="glyphicon glyphicon-cog"></span>&nbsp;&nbsp;Opzioni</a></p>
            </div>
          </div>
          <div class="col-sm-10 text-left contentView">
            <h1><%= session.getAttribute("cognome")%> <%= session.getAttribute("nome")%></h1>
            <p style="padding-left: 10px;">
              <span style="color: blue;">Mail</span>&nbsp;&nbsp;<%= session.getAttribute("username")%><br>
              <span style="color: blue;">Matr</span>&nbsp;<%=session.getAttribute("matricola")%>
            </p>
            <hr>
            <p style="padding-left: 10px;">
              <span style="color: blue;">Prov</span>&nbsp;&nbsp;&nbsp;<%= session.getAttribute("provincia")%><br>
              <span style="color: blue;">Ruolo</span>&nbsp;<%= session.getAttribute("ruolo")%><br>
            </p>
            <hr>
            <%
              Database db9 = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
              Connection conn9 = db9.getConnection();
              PreparedStatement st9 = conn9.prepareStatement("SELECT nome, cognome, email, codice_fiscale, luogo_nascita, data_nascita FROM Pazienti WHERE email= ? OR codice_fiscale= ?");
              st9.setString(1, request.getParameter("paziente"));
              st9.setString(2, request.getParameter("paziente"));
              ResultSet rs9 = st9.executeQuery();
              if(!rs9.next()) {
                response.sendRedirect("ListaPazienti");
              }
              else {
            %>
              <div class="row" style="margin-bottom: 10px;">
                <div class="col-sm-6 text-left">
                  <h3><%= rs9.getString(1)%> <%= rs9.getString(2)%><span>&nbsp;-&nbsp;(<%= rs9.getString(4)%>)</span></h3>
                  <span style="padding: 10px;"><%= rs9.getString(6)%> (<%= rs9.getString(5)%>)</span><br>
                </div>
                <div class="col-sm-6 text-center" style="max-height: 200px;">
                  <br>
                  <form method="GET" action="NuovaVisita" class="customInline" style="margin-bottom: 2px;">
                    <button type="submit" class="btn btn-danger" style="width: 140px; padding: 10px; margin-bottom: 10px; background-color: red;" name="paziente" value="<%= rs9.getString(3)%>">Crea visita</button>
                  </form>
                  <form method="GET" action="NuovoEsame" class="customInline" style="margin-bottom: 2px;">
                    <button type="submit" class="btn btn-danger" style="width: 140px; padding: 10px; margin-bottom: 10px; background-color: red;" name="paziente" value="<%= rs9.getString(3)%>">Prescrivi esame</button>
                  </form>
                  <form method="GET" action="NuovaRicetta" class="customInline" style="margin-bottom: 2px;">
                    <button type="submit" class="btn btn-danger" style="width: 140px; padding: 10px; margin-bottom: 10px; background-color: red;" name="paziente" value="<%= rs9.getString(3)%>">Crea ricetta</button>
                  </form>
                </div>
              </div>
                  
              <div class="row">
                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#visiteP" style="color: coral;">Visite</a></li>
                  <li><a data-toggle="tab" href="#esami" style="color: teal;">Esami</a></li>
                  <li><a data-toggle="tab" href="#ricette" style="color: firebrick;">Ricette</a></li>
                </ul>
                <br>
                <div class="tab-content">
                  <div id="visiteP" class="tab-pane fade in active" style="margin-left: 10px;">
                    <table class="table table-bordered table-striped" style="width: 90%;">
                      <thead style="background-color: coral;">
                        <tr>
                          <th style="text-align: center;">ID</th>
                          <th style="text-align: center;">Medico di Base</th>
                          <th style="text-align: center;">Data</th>
                          <th style="text-align: center;">Info</th>
                        </tr>
                      </thead>
                      <tbody id="myTable" style="text-align: center;">
                        <!--HERE-->
                        <%
                          Database db2 = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
                          Connection conn2 = db2.getConnection();
                          PreparedStatement st2 = conn2.prepareStatement("SELECT V.id, M.matricola, V.data, V.note FROM MediciB AS M, Visita AS V, Pazienti as P WHERE M.email=V.id_medico AND P.email = V.id_paziente AND (P.email= ? OR P.codice_fiscale= ?)");
                          st2.setString(1, request.getParameter("paziente"));
                          st2.setString(2, request.getParameter("paziente"));
                          ResultSet rs2 = st2.executeQuery();
                          while(rs2.next()) {
                        %>
                          <tr>
                            <td><%= rs2.getInt(1)%></td>
                            <td><%= rs2.getString(2)%></td>
                            <td><%= rs2.getDate(3)%></td>
                            <td>
                              <button class="Info" onclick="infoVisita('<%= rs2.getInt(1)%>', '<%= rs2.getString(4)%>')"><span class="glyphicon glyphicon-file"></span></button>
                            </td>
                          </tr>
                        <%  
                          }
                        %>
                      </tbody>
                    </table>
                  </div>
                  
                  <div id="esami" class="tab-pane fade" style="margin-left: 10px;">
                    <table class="table table-bordered table-striped" style="width: 90%;">
                      <thead style="background-color: teal;">
                        <tr>
                          <th style="text-align: center;">ID</th>
                          <th style="text-align: center;">Esame</th>
                          <th style="text-align: center;">Medico B</th>
                          <th style="text-align: center;">Data presc.</th>
                          <th style="text-align: center;">Status</th>
                          <th style="text-align: center;">Data E</th>
                          <th style="text-align: center;">Referto</th>
                        </tr>
                      </thead>
                      <tbody id="myTable" style="text-align: center;">
                        <!--HERE-->
                        <%
                          Database db6 = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
                          Connection conn6 = db6.getConnection();
                          PreparedStatement st6 = conn6.prepareStatement("SELECT Pr.id, E.nome, Pr.dataV, Pr.erogato, Pr.data, Pr.referto, E.id, M.matricola FROM Prescrizioni AS Pr, Pazienti as P, Esami AS E, MediciB AS M WHERE M.email=Pr.id_medicoB AND P.email = Pr.id_paziente AND Pr.id_esame = E.id AND (P.email= ? OR P.codice_fiscale= ?)");
                          st6.setString(1, request.getParameter("paziente"));
                          st6.setString(2, request.getParameter("paziente"));
                          ResultSet rs6 = st6.executeQuery();
                          while(rs6.next()) {
                        %>
                          <tr>
                            <td><%= rs6.getInt(1)%></td>
                            <td><%= rs6.getInt(7)%>-<%= rs6.getString(2)%></td>
                            <td><%= rs6.getString(8)%></td>
                            <td><%= rs6.getDate(3)%></td>
                            <td>
                              <% if(rs6.getBoolean(4)) { %>
                                <span style="color: green;">erogato</span>
                              <% } else { %>
                                <span style="color: red;">da erogare</span>
                              <% } %>
                            </td>
                            <td>
                              <% if(rs6.getBoolean(4)) { %>
                                <span style="color: green;"><%= rs6.getDate(5)%></span>
                              <% } else { %>
                                <span style="color: grey;">da erogare</span>
                              <% } %>
                            </td>
                            <td>
                              <% if(rs6.getBoolean(4)) {
                                  String content = "";
                                  if(rs6.getString(6)!=null)
                                    content = rs6.getString(6);
                              %>
                                <button class="Info" onclick="infoEsame('<%= rs6.getInt(1)%>', '<%= content%>')"><span class="glyphicon glyphicon-file"></span></button>
                              <%}%>
                            </td>
                          </tr>
                        <%  
                          }
                        %>
                      </tbody>
                    </table>
                  </div>
                      
                  <div id="ricette" class="tab-pane fade" style="margin-left: 10px;">
                    <table class="table table-bordered table-striped" style="width: 90%;">
                      <thead style="background-color: firebrick;">
                        <tr>
                          <th style="text-align: center;">Ricetta</th>
                          <th style="text-align: center;">ID F</th>
                          <th style="text-align: center;">Farmaco</th>
                          <th style="text-align: center;">Quantita'</th>
                          <th style="text-align: center;">Medico di Base</th>
                          <th style="text-align: center;">Status Erogato</th>
                          <th style="text-align: center;">Note</th>
                        </tr>
                      </thead>
                      <tbody id="myTable" style="text-align: center;">
                        <!--HERE-->
                        <%
                          Database db7 = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
                          Connection conn7 = db7.getConnection();
                          PreparedStatement st7 = conn7.prepareStatement("SELECT R.id, R2.id_farmaco, F.nome, R2.quantita, R.erogata, R.descrizione, M.matricola FROM RicTot AS R, Ricette2 AS R2, Pazienti AS P, Farmaci AS F, MediciB AS M WHERE P.email=R.id_paziente AND R2.id_farmaco=F.id AND R2.id_ric=R.id AND M.email=R.id_medico AND (P.email= ? OR P.codice_fiscale= ?)");
                          st7.setString(1, request.getParameter("paziente"));
                          st7.setString(2, request.getParameter("paziente"));
                          ResultSet rs7 = st7.executeQuery();
                          while(rs7.next()) {
                        %>
                          <tr>
                            <td><%= rs7.getInt(1)%></td>
                            <td><%= rs7.getInt(2)%></td>
                            <td><%= rs7.getString(3)%></td>
                            <td><%= rs7.getInt(4)%></td>
                            <td><%= rs7.getString(7)%></td>
                            <td>
                              <% if(rs7.getBoolean(5)) {%>
                                <span style="color: green;">erogato</span>
                              <% } else {%>
                                <span style="color: red;">da erogare</span>
                              <% }%>
                            </td>
                            <td>
                              <button class="Info" onclick="infoRicetta('<%= rs7.getInt(1)%>', '<%= rs7.getString(6)%>')"><span class="glyphicon glyphicon-file"></span></button>
                            </td>
                          </tr>
                        <%  
                          }
                        %>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>   
            <% } %>
          </div>
        </div>
        <br><br><br>
      <% }
        } catch(Exception e) {
          response.sendRedirect("/");
        }
      %>   
    </div>
    
    <nav class="navbar navbar-inverse navbar-fixed-bottom">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">Servizi Sanitari</a>
        </div>
      </div>
    </nav>  

    <script>
      var nomi = [];
      var cognomi = [];
      var email = [];
      var cf = [];
      var citta = [];
      <%
        try {
          Database db3 = Database.getInstance();   //Questa è l'istanza del database che serve per ottenere la connessione
          Connection conn3 = db3.getConnection();
          PreparedStatement st3 = conn3.prepareStatement("SELECT nome, cognome, email, codice_fiscale, luogo_nascita FROM Pazienti WHERE id_medico = ?");
          st3.setString(1, ((String) session.getAttribute("username")));
          ResultSet rs3 = st3.executeQuery();
          while(rs3.next()) {
      %>
        nomi.push("<%=rs3.getString(1)%>");
        cognomi.push("<%=rs3.getString(2)%>");
        email.push("<%=rs3.getString(3)%>");
        cf.push("<%=rs3.getString(4)%>");
        citta.push("<%=rs3.getString(5)%>");
      <%
          }
        } catch(Exception e) {
          response.sendRedirect("/");
        }
      %>
      autocomplete(document.getElementById("myInput"), nomi,cognomi,email,cf,citta);
    </script>
    
  </body>
</html>