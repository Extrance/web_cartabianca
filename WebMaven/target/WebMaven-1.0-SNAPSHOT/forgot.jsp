<%-- 
    Document   : forgot
    Created on : 22-ott-2019, 18.50.43
    Author     : andreabalasso
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Servizi sanitari - Reset Password</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="icon" href="img/transparent.png" type="image/png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="css/sidenav.css" type="text/css"/>
    <link rel="stylesheet" href="css/autocompleteCss.css" type="text/css"/>
    <style>
      body{
        background-image: url(img/background.jpg);
        background-position: top;
        background-attachment: fixed;
        background-repeat: no-repeat;
      }
    </style>
  </head>
  <body onload="funP(); funMB(); funMS();" data-spy="scroll" data-target=".navbar" data-offset="50">
    <!--Navigation bar-->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><img src="img/transparent.png" width="40px" style="margin-top: -9.5px; "></a>
        </div>
        <div class="collapse navbar-collapse"  style="padding-left:30px;" id="myNavbar">
          <ul class="nav navbar-nav">
            <li><a href="/#section1">Chi siamo</a></li>
            <li><a href="/#section2">Servizi</a></li>
            <li><a href="/#section3">Dove trovarci</a></li>
          </ul>
          <% if(session.getAttribute("username")==null) { %>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a>
                <ul class="dropdown-menu">
                  <li><a href="#" data-toggle="modal" data-target="#loginP">Paziente</a></li>
                  <li><a href="#" data-toggle="modal" data-target="#loginMB">Medico di Base</a></li>
                  <li><a href="#" data-toggle="modal" data-target="#loginMS">Medico Specialista</a></li>
                </ul>
              </li>
            </ul>
          <% } %>
        </div>
      </div>
    </nav>
        
    <!--Form Login Paziente-->
    <div class="modal fade" id="loginP" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <form method="POST" action="LoginPaziente" name="TestFormP">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Login Paziente</h4>
            </div>
            <div class="modal-body">
              <input type="text" placeholder="Email" name="username" style="width: 250px;"><br><br>
              <input type="password" placeholder="Password" name="password" style="width: 250px;"><br>
              <input type="checkbox" name="rememberme" value="rememberme"> Ricordami<br><br>
              <a href="ForgotPassword" style="color: hotpink;">Password Dimenticata</a>
              <script type="text/javascript">
                function funP() {
                  if (document.cookie != ""){
                    cookies = document.cookie.split(";");
                    for (var i = 0; i < cookies.length; i++) {
                      cookie = cookies[i].trim().split("=");
                      if (cookie[0] == "usernamePaziente") {
                        cookie = cookies[i].trim().split("\"");
                        document.TestFormP.username.value = cookie[1];
                      }
                      if (cookie[0] == "passwordPaziente") {
                        document.TestFormP.password.value = cookie[1];
                      }
                    }
                  }
                }    
              </script>
            </div>
            <div class="modal-footer">
              <input type="submit" class="btn btn-primary" value="Accedi"><br>
            </div>
          </form>
        </div>
      </div>
    </div>
    
    <!--Form Login Medico di Base-->
    <div class="modal fade" id="loginMB" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <form method="POST" action="LoginMedicoBase" name="TestFormMB">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Login Medico di Base</h4>
            </div>
            <div class="modal-body">
              <input type="text" placeholder="Email" name="username" style="width: 250px;"><br><br>
              <input type="password" placeholder="Password" name="password" style="width: 250px;"><br>
              <input type="checkbox" name="rememberme" value="rememberme"> Ricordami<br><br>
              <a href="ForgotPassword" style="color: hotpink;">Password Dimenticata</a>
              <script type="text/javascript">                 
                function funMB() {
                    if (document.cookie != ""){
                        cookies = document.cookie.split(";");
                         for (var i = 0; i < cookies.length; i++) {
                            cookie = cookies[i].trim().split("=");
                            if (cookie[0] == "usernameMB") {
                                cookie = cookies[i].trim().split("\"");
                                document.TestFormMB.username.value = cookie[1];
                            }
                            if (cookie[0] == "passwordMB") {
                                document.TestFormMB.password.value = cookie[1];
                            }
                        }
                    }
                }    
              </script>
            </div>
            <div class="modal-footer">
              <input type="submit" class="btn btn-primary" value="Accedi"><br>
            </div>
          </form>
        </div>
      </div>
    </div>
    
    <!--Form Login Medico Specialista-->
    <div class="modal fade" id="loginMS" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <form method="POST" action="LoginMedicoSpecialista" name="TestFormMS">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Login Medico Specialista</h4>
            </div>
            <div class="modal-body">
              <input type="text" placeholder="Email" name="username" style="width: 250px;"><br><br>
              <input type="password" placeholder="Password" name="password" style="width: 250px;"><br>
              <input type="checkbox" name="rememberme" value="rememberme"> Ricordami<br><br>
              <a href="ForgotPassword" style="color: hotpink;">Password Dimenticata</a>
              <script type="text/javascript">                 
                function funMS() {
                    if (document.cookie != ""){
                        cookies = document.cookie.split(";");
                        for (var i = 0; i < cookies.length; i++) {
                            cookie = cookies[i].trim().split("=");
                            if (cookie[0] == "usernameMS") {
                                cookie = cookies[i].trim().split("\"");
                                document.TestFormMS.username.value = cookie[1];
                            }
                            if (cookie[0] == "passwordMS") {
                                document.TestFormMS.password.value = cookie[1];
                            }
                        }
                    }
                }    
              </script>
            </div>
            <div class="modal-footer">
              <input type="submit" class="btn btn-primary" value="Accedi"><br>
            </div>
          </form>
        </div>
      </div>
    </div>
    
    <!--Page Content-->
    <div class="container-fluid">
      <div class="row">
        <br><br><br><br>
        <div class="col-sm-12 text-center">
          <% if(session.getAttribute("username")!=null) { %>
            <h1>Attenzione: Login gia' effettuato</h1>
            <a href="/">Menu Principale</a>
          <% } else { %>
            <form action="ForgotPassword" method="POST">
              <p><input type="text" style="width:250px;" name="username" placeholder="Username" required/></p>
              <p><input type="text" style="width:250px;" name="cf" placeholder="Codice Fiscale o Matricola" required/></p>
              <hr>
              <h3>--Ruolo--</h3>
              <p>
                <input type="radio" name="ruolo" value="paziente" checked>Paziente<br>
                <input type="radio" name="ruolo" value="medicoB">Medico di Base<br>
                <input type="radio" name="ruolo" value="medicoS">Medico Specialista
              </p>
              <input type="submit" class="btn btn-primary" value="Submit"/>
            </form>
          <% } %>
        </div>
      </div>
      <br><br><br>
    </div>
    
    <nav class="navbar navbar-inverse navbar-fixed-bottom">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">Servizi Sanitari</a>
        </div>
      </div>
    </nav>    
  </body>
</html>