<%-- 
    Document   : noPaziente
    Created on : 21-ott-2019, 21.47.12
    Author     : andreabalasso
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    
    <title>Servizi sanitari</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"></link>
    <link rel="icon" href="img/transparent.png" type="image/png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="css/sidenav.css" type="text/css"/>
    <style>
      body{
        background-image: url(img/background.jpg);
        background-position: top;
        background-attachment: fixed;
        background-repeat: no-repeat;
      }
    </style>
  </head>
  <body>
        
    <!--Page Content-->
    <div class="container-fluid text-center">
      <br><br>
      <div class="row content">
        <div class="col-sm-12 text-left">
          <% if(session.getAttribute("ruolo")==null) { %>
            <h1>Attenzione: Login non effettuato</h1>
            <p>Si prega di effettuare il login per accedere al contenuto della pagina richiesta</p>
            <a href="/" class="btn btn-link">Menu Principale</a>
          <% } else { %>
            <h1>Attenzione: Ruolo non corretto</h1>
            <p>Presente un'altra sessione attiva per un ruolo diverso</p>
            <p>Si prega di effettuare il login per il ruolo desiderato per accedere al contenuto della pagina richiesta</p>
            <a href="/" class="btn btn-link">Menu Principale</a>
          <% } %>
        </div>
      </div>
      <br><br><br>
    </div>
    
    <nav class="navbar navbar-inverse navbar-fixed-bottom">
      <footer class="container-fluid text-center">
        <p>Servizi Sanitari</p>
      </footer>
    </nav>    
  </body>
</html>