<%-- 
    Document   : privateLogin
    Created on : 14-dic-2019, 12.01.10
    Author     : andreabalasso
--%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="it.unitn.aa1718.webprogramming.webmaven.DataAccess.Database"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    
    <title>Servizi sanitari - Area Riservata</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"></link>
    <link rel = "icon" href ="img/transparent.png" type = "image/png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="css/sidenav.css" type="text/css"/>
    <style>
      body {
        position: relative;
      }
      
      .customHoverable:hover {
        opacity: 0.5;
      }
      
      input {
        border: 1px solid transparent;
        background-color: #f1f1f1;
        padding: 10px;
        font-size: 16px;
      }
      
      input[type=text] {
        background-color: #f1f1f1;
        width: 100%;
      }
    </style>
  </head>
  <body>
    
    <%  if(session.getAttribute("errorMessage")!=null) {%>
      <script>
        window.alert("Errore: "+"<%= session.getAttribute("errorMessage")%>");
      </script>
    <%
          session.setAttribute("errorMessage", null);
        }
    %>
    
    <!--Page Content-->
    <%  
      if(session.getAttribute("ruolo")!=null && session.getAttribute("ruolo")!="Admin") { 
        request.getRequestDispatcher("noCorrectLogin.jsp").forward(request, response);
      } else {
    %>
    <div class="container-fluid">
      <div class="row" style="text-align: center; margin-top: 80px;">
        <%if(session.getAttribute("ruolo")==null) {%>
          <span style="margin-left: 10px; margin-right: 10px;">Inserisci le credenziali per accedere all'area riservata</span><br>
          <form action="ReservedLogin" method="POST">
            <input type="text" name="idOperatore" placeholder="ID Operatore" style="width: 250px; margin: 20px;" required><br>
            <input type="password" name="password" placeholder="Password" style="width: 250px; margin-bottom: 20px;" required><br>
            <input type="submit" name="Login" value="Login" class="btn btn-primary customHoverable" style="width: 100px;">
          </form>
        <% } else {%>
          <form action="Slog" method="POST">
            <button type="submit" class="btn btn-primary" name="caller" value="LogOut">Logout</button>
          </form>
        <% } %>
      </div>
      <br><br><br>
    </div>
    <% } %>
    
    <nav class="navbar navbar-inverse navbar-fixed-bottom">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">Servizi Sanitari</a>
        </div>
      </div>
    </nav>
    
    <script type="text/javascript" id="cookieinfo" src="//cookieinfoscript.com/js/cookieinfo.min.js"></script>
    
  </body>
</html>