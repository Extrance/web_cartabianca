-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: ServiziSanitari
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `MediciB`
--

DROP TABLE IF EXISTS `MediciB`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MediciB` (
  `email` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `cognome` varchar(30) NOT NULL,
  `citta` varchar(30) NOT NULL,
  `provincia` char(2) NOT NULL,
  `matricola` varchar(30) NOT NULL,
  PRIMARY KEY (`email`),
  KEY `provincia` (`provincia`),
  CONSTRAINT `medicib_ibfk_1` FOREIGN KEY (`provincia`) REFERENCES `Ssp` (`provincia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MediciB`
--

LOCK TABLES `MediciB` WRITE;
/*!40000 ALTER TABLE `MediciB` DISABLE KEYS */;
INSERT INTO `MediciB` VALUES ('francescosandri@gmail.com','migliorMEDICO1964','Francesco','Sandri','Piombino Dese','PD','PD00001'),('giuliogallo@gmail.com','MastroLindo57','Giulio','Gallo','Romano di Ezzelino','VI','VI00001'),('iu3byv@gmail.com','CiaoMB','Andrea','Balasso','Schio','VI','VI00004'),('lucacarta@gmail.com','darkGAMER967','Luca','Carta','Marostica','VI','VI00002'),('marcorossi@gmail.com','PINCOpallino25','Marco','Rossi','Bassano del Grappa','VI','VI00003'),('robertafarina@gmail.com','auroraMARIO1228','Roberta','Farina','San Martino di Lupari','PD','PD00002');
/*!40000 ALTER TABLE `MediciB` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-31 10:32:22
