-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: ServiziSanitari
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Prescrizioni`
--

DROP TABLE IF EXISTS `Prescrizioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Prescrizioni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_esame` int(11) NOT NULL,
  `id_paziente` varchar(30) NOT NULL,
  `referto` text,
  `id_medicoB` varchar(30) NOT NULL,
  `id_medicoS` varchar(30) DEFAULT NULL,
  `erogato` tinyint(1) NOT NULL DEFAULT '0',
  `ticket` tinyint(1) NOT NULL DEFAULT '0',
  `dataV` date DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_esame` (`id_esame`),
  KEY `id_medicoS` (`id_medicoS`),
  KEY `id_medicoB` (`id_medicoB`),
  KEY `id_paziente` (`id_paziente`),
  CONSTRAINT `prescrizioni_ibfk_1` FOREIGN KEY (`id_esame`) REFERENCES `Esami` (`id`),
  CONSTRAINT `prescrizioni_ibfk_2` FOREIGN KEY (`id_medicoS`) REFERENCES `MediciS` (`email`),
  CONSTRAINT `prescrizioni_ibfk_3` FOREIGN KEY (`id_medicoB`) REFERENCES `MediciB` (`email`),
  CONSTRAINT `prescrizioni_ibfk_4` FOREIGN KEY (`id_paziente`) REFERENCES `Pazienti` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Prescrizioni`
--

LOCK TABLES `Prescrizioni` WRITE;
/*!40000 ALTER TABLE `Prescrizioni` DISABLE KEYS */;
INSERT INTO `Prescrizioni` VALUES (1,8,'iu3byv@gmail.com',NULL,'iu3byv@gmail.com',NULL,0,0,'2019-12-20',NULL),(2,1,'iu3byv@gmail.com','222222','iu3byv@gmail.com','iu3byv@gmail.com',1,0,'2019-12-20','2019-12-20'),(3,1,'balasso.andrea69@gmail.com',NULL,'giuliogallo@gmail.com',NULL,0,0,'2019-12-20',NULL),(4,3,'iu3byv@gmail.com','11111','iu3byv@gmail.com','iu3byv@gmail.com',1,1,'2019-12-20','2019-12-20'),(5,3,'iu3byv@gmail.com',NULL,'iu3byv@gmail.com',NULL,0,0,'2019-12-20',NULL),(6,11,'iu3byv@gmail.com','ok tick','iu3byv@gmail.com','iu3byv@gmail.com',1,1,'2019-12-21','2019-12-21'),(7,1,'iu3byv@gmail.com','ok prov','iu3byv@gmail.com','iu3byv@gmail.com',1,0,'2019-12-21','2019-12-21'),(8,2,'balasso.andrea69@gmail.com',NULL,'giuliogallo@gmail.com',NULL,0,0,'2019-12-21',NULL);
/*!40000 ALTER TABLE `Prescrizioni` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-31 10:32:22
